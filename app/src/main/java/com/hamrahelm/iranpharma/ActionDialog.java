package com.hamrahelm.iranpharma;

import android.app.Activity;
import android.app.Dialog;
import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.StringRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.IntentCompat;

import com.google.android.material.button.MaterialButton;

public class ActionDialog extends AppCompatActivity {
    TextView nuText;
    MaterialButton nuOk, nuCancel;
    private Activity activity;
    private Dialog dialog;


    public ActionDialog(@StringRes int text, String type, String url, Activity activity) {
        this.activity = activity;

        setDialog();
        findViews();
        setData(text, type, url);
    }


    public void showDialog() {
        dialog.show();
    }

    public void dismiss() {
        dialog.dismiss();
    }


    private void setDialog() {
        dialog = new Dialog(activity);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_update);
    }

    private void findViews() {
        RelativeLayout r1 = dialog.findViewById(R.id.r1);
        nuOk = dialog.findViewById(R.id.nuOk);
        nuCancel = dialog.findViewById(R.id.nuCancel);
        nuText = dialog.findViewById(R.id.nuText);

        Tools.setFont(nuText);
        Tools.setFont(nuOk);
        Tools.setFont(nuCancel);
//        tvTitle = dialog.findViewById(R.id.tv_title);
//        tvSubtitle = dialog.findViewById(R.id.tv_subtitle);
//        tvAction = dialog.findViewById(R.id.tv_action);
    }

    private void setData(int text, String type, final String url) {
        nuText.setText(text);

        if (type.equals("soft")) {
            nuCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(activity, ActionDialog.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            });
            nuOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Uri url = Uri.parse("http://www.someUrl.com");
                    Intent intent = new Intent(Intent.ACTION_VIEW, url);
                    startActivity(intent);
                }
            });
        }
        if (type.equals("hard")) {
            nuCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
            nuOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
        }
    }

}
