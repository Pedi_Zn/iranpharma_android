package com.hamrahelm.iranpharma.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.hamrahelm.iranpharma.DrugFragment;
import com.hamrahelm.iranpharma.R;
import com.hamrahelm.iranpharma.Tools;
import com.hamrahelm.iranpharma.model.Category;
import com.hamrahelm.iranpharma.model.Library;
import com.hamrahelm.iranpharma.network.RetrofitClientInstance;

import java.util.List;

public class CategoryAdapter extends BaseAdapter {

    public List<Library> libraryList;
    public Context context;
    private ViewHolder vh;

    public CategoryAdapter(Context context, List<Library> libraryList) {
        this.context = context;
        this.libraryList = libraryList;
    }

    public int getCount() {
        return libraryList.size();
    }

    public Library getItem(int position) {
        return libraryList.get(position);
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView != null) {
            vh = (ViewHolder) convertView.getTag();
        } else {
            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.list_item, parent, false);

            vh = new ViewHolder();
            vh.r1 = convertView.findViewById(R.id.r1);
            vh.title = convertView.findViewById(R.id.title);
            vh.image = convertView.findViewById(R.id.image);
            vh.inside = convertView.findViewById(R.id.inside);

            convertView.setTag(vh);
        }

        Library library = getItem(position);
        if (library != null) {
            vh.title.setText(library.getTitle());

            Glide.with(context)
                    .load(RetrofitClientInstance.BASE_URL + library.getIconLibrary())
                    .apply(new RequestOptions()
                            .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                            .diskCacheStrategy(DiskCacheStrategy.NONE))
                    .transition(DrawableTransitionOptions.withCrossFade(150))
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    })
                    .into(vh.image);

        }

        String textColor = "";
        String textStyle;
        int visibility;

        if (library.getModel().equals("header")) {
            textColor = "364FB3";
            visibility = View.GONE;
            Tools.setBoldFont(vh.title);

        }
        else {
            textColor = "494949";
            visibility = View.VISIBLE;
            Tools.setFont(vh.title);
        }

        vh.title.setTextColor(Color.parseColor("#" + textColor));
        vh.image.setVisibility(visibility);
        vh.inside.setVisibility(visibility);

        return convertView;
    }

    private class ViewHolder {
        RelativeLayout r1;
        TextView title;
        ImageView inside;
        ImageView image;
    }

}
