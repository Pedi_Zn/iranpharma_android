package com.hamrahelm.iranpharma.adapter;

import android.content.Context;
import android.graphics.Color;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.hamrahelm.iranpharma.R;
import com.hamrahelm.iranpharma.Tools;

import java.util.ArrayList;
import java.util.HashMap;

public class DrugListAdapter extends BaseAdapter {

    private Animation animationUp;
    private Animation animationDown;
    public ArrayList<HashMap<String, String>> drugList;

    public Context context;
    public ListView listView;
    public int drugSize;
    public ArrayList<Integer> cardFieldsStat = new ArrayList();

    public DrugListAdapter(Context context, ArrayList<HashMap<String, String>> drugList, ListView listView, int drugSize) {
        this.context = context;
        this.drugList = drugList;
        this.listView = listView;
        this.drugSize = drugSize;
    }

    public int getCount() {
        return drugList.size();
    }

    public HashMap<String, String> getItem(int position) {
        return drugList.get(position);
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final DrugListAdapter.ViewHolder vh;

        if (convertView != null) {
            vh = (DrugListAdapter.ViewHolder) convertView.getTag();
        } else {
            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.list_item_drug, parent, false);
            vh = new DrugListAdapter.ViewHolder();
            vh.title = convertView.findViewById(R.id.title);
            vh.description = convertView.findViewById(R.id.description);
            vh.r1 = convertView.findViewById(R.id.r1);

            animationUp = AnimationUtils.loadAnimation(context, R.anim.slide_up);
            animationDown = AnimationUtils.loadAnimation(context, R.anim.slide_down);

            Tools.setFont(vh.title);
            Tools.setFont(vh.description);

            vh.title.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (vh.description.isShown()) {
//                        vh.description.startAnimation(animationUp);
                        vh.description.setVisibility(View.GONE);
//                        vh.description.removeAllViews();
                        cardFieldsStat.set(position, 0);
                        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) listView.getLayoutParams();
                        Integer sumHeight = 0;
                        for (int i = 0; i < cardFieldsStat.size(); i++) {
                            sumHeight = sumHeight + cardFieldsStat.get(i);
                        }
                        params.height = 220 * drugSize + sumHeight;
                        listView.setLayoutParams(params);

                    } else {
                        vh.description.setVisibility(View.VISIBLE);
//                        vh.description.startAnimation(animationDown);

                        vh.r1.post(new Runnable() {
                            public void run() {
                                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) listView.getLayoutParams();
                                cardFieldsStat.set(position, vh.description.getHeight());
                                Integer sumHeight = 0;
                                for (int i = 0; i < cardFieldsStat.size(); i++) {
                                    sumHeight = sumHeight + cardFieldsStat.get(i);
                                }
                                params.height = 220 * drugSize + sumHeight;
                                listView.setLayoutParams(params);
                            }
                        });

                    }

                }
            });

            convertView.setTag(vh);
        }

        HashMap<String, String> drugCard = getItem(position);
        String key = "", value = "";
        if (drugCard != null) {
            if (drugCard.keySet().toString().startsWith("[") && drugCard.keySet().toString().endsWith("]")) {
                key = drugCard.keySet().toString().substring(1, drugCard.keySet().toString().length() - 1);
            }
            if (drugCard.values().toString().startsWith("[") && drugCard.values().toString().endsWith("]")) {
                value = drugCard.values().toString().substring(1, drugCard.values().toString().length() - 1);
            }

//
            vh.title.setText(key);
            convertHtmltoXml(key, value, vh);

//            vh.description.setText(key);
//            vh.description.setText(Html.fromHtml(value));

            if (key.equals(context.getString(R.string.sources))) {
                TextView textView = new TextView(context);
                textView.setText(Html.fromHtml("<p class=\"ql-align-right\" >" + value + "</p>"));
                textView.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                Tools.setFont(textView);
                vh.description.removeAllViews();
                vh.description.addView(textView);
            }
            cardFieldsStat.add(0);
        }

        return convertView;
    }


    public void convertHtmltoXml(String key, String text, DrugListAdapter.ViewHolder vh) {
        TextView textView = new TextView(context);

        textView.setText(text);
        textView.setTextColor(context.getResources().getColor(R.color.white));
        String mode = "p";
        String rowString = "";
        String tempData = "";
        int rowNumber = 0;
        TableRow rowData = new TableRow(context);
        TableLayout tempTable = new TableLayout(context);
        String[] splitedText = text.split("</");
        for (int i = 0; i < splitedText.length; i++) {


            if (splitedText[i].startsWith("p")) {
                splitedText[i] = splitedText[i].substring(2);
                if (!mode.equals("table")) {
                    TextView t2 = new TextView(context);
                    Tools.setFont(t2);
                    t2.setText(Html.fromHtml(tempData + "</p>"));
                    t2.setTextColor(Color.BLACK);
                    vh.description.removeAllViews();
                    vh.description.addView(t2);
//                    tempData = "";
                } else {
                    tempData += "</p>" + splitedText[i];
//                    tempData = tempData.replace("lang=", "style=\"line-height:0px;\" lang=");
                }
            } else if (splitedText[i].startsWith("tbody")) {
                mode = "";
                rowNumber = 0;
                HorizontalScrollView horizontalScrollView = new HorizontalScrollView(context);
                horizontalScrollView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tempTable.setStretchAllColumns(true);
                tempTable.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                horizontalScrollView.addView(tempTable);
                vh.description.removeAllViews();
                vh.description.addView(horizontalScrollView);
                tempTable = new TableLayout(context);
            } else if (splitedText[i].startsWith("tr")) {
                splitedText[i] = splitedText[i].substring(3);
                rowData.setLayoutParams(new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT));
                if (rowNumber == 0) {
                    rowData.setBackground(context.getResources().getDrawable(R.drawable.row_rounded_top));
                    rowData.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
                } else {
                    rowData.setBackgroundColor(context.getResources().getColor(R.color.colorPrimaryLight));
                }
                rowData.setId(i);
                rowData.setPadding(10, 50, 10, 0);
                rowData.setHorizontalGravity(Gravity.CENTER_HORIZONTAL);
                rowData.setVerticalGravity(Gravity.CENTER_VERTICAL);
                tempTable.addView(rowData);
                rowNumber++;
                Log.d("ROW DATA : ", String.valueOf(rowData));

                rowData = new TableRow(context);
                // add row to tempTable
            } else if (splitedText[i].startsWith("td")) {
                TextView label_date1 = new TextView(context);
                Tools.setFont(label_date1);
                label_date1.setId(i + 1);
                label_date1.setText(Html.fromHtml(tempData));
                label_date1.setTextColor(Color.BLACK);
                label_date1.setMaxWidth(600);
                label_date1.setLineSpacing(35, 0);
                label_date1.setGravity(Gravity.CENTER_HORIZONTAL);
                label_date1.setPadding(35, 1, 15, 1);
                label_date1.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                label_date1.setBackground(context.getResources().getDrawable(R.drawable.row_rounded_top));

                if (rowNumber == 0) {
                    label_date1.setMaxLines(1);
                    label_date1.setHeight(155);
                    label_date1.setTextColor(Color.WHITE);
                    label_date1.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
                } else {
                    label_date1.setMaxLines(1);
                    label_date1.setMaxHeight(200);
                    label_date1.setTextColor(Color.BLACK);
                    label_date1.setBackgroundColor(context.getResources().getColor(R.color.colorPrimaryLight));
                }
                rowData.addView(label_date1);// add the column to the table row here
                splitedText[i] = splitedText[i].substring(3);
                rowString += tempData + "\n Z : ";
                tempData = "";

                // add Text View to Row
            } else if (splitedText[i].startsWith("h4")) {
                TextView t2 = new TextView(context);
                Tools.setFont(t2);
                t2.setText(Html.fromHtml(tempData + "</h4>"));
                t2.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                vh.description.removeAllViews();
                vh.description.addView(t2);
                splitedText[i] = splitedText[i].substring(3);
                tempData = "";
            } else if (splitedText[i].startsWith("h5")) {
                TextView t2 = new TextView(context);
                Tools.setFont(t2);
                t2.setText(Html.fromHtml(tempData + "</h5>"));
                t2.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                vh.description.removeAllViews();
                vh.description.addView(t2);
                splitedText[i] = splitedText[i].substring(3);
                tempData = "";
            } else if (splitedText[i].startsWith("h3")) {
                TextView t2 = new TextView(context);
                Tools.setFont(t2);
                t2.setText(Html.fromHtml(tempData + "</h3>"));
                t2.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                vh.description.removeAllViews();
                vh.description.addView(t2);
                splitedText[i] = splitedText[i].substring(3);
                tempData = "";
            } else if (splitedText[i].startsWith("strong")) {
                tempData += "</" + splitedText[i];
                if (mode.equals("bold")) {
                    TextView t2 = new TextView(context);
                    Tools.setFont(t2);
                    t2.setText(Html.fromHtml(tempData));

                    vh.description.removeAllViews();
                    vh.description.addView(t2);
                    tempData = "";
                    mode = "p";
                } else {
                    splitedText[i] = splitedText[i].substring(7);
                }


            } else if (splitedText[i].startsWith("span")) {
                tempData += "</" + splitedText[i];
                splitedText[i] = splitedText[i].substring(5);
            } else {

            }

            if (splitedText[i].startsWith("<p") || splitedText[i].startsWith("<h4") || splitedText[i].startsWith("<h5") || splitedText[i].startsWith("<h3")) {

                if (!mode.equals("table")) {
                    mode = "p";
                } else {

                    if (splitedText[i].startsWith("<p")) {
                        splitedText[i] = splitedText[i].replace("lang=", "style=\"line-height:0px;\" lang=");
                    }
                }

                tempData += splitedText[i];

            } else if (splitedText[i].contains("<tbody")) {

                tempTable = new TableLayout(context);
                mode = "table";
                rowNumber = 0;
                if (splitedText[i].contains("<tr")) {
                    rowData = new TableRow(context);
                }
                if (splitedText[i].contains("<td")) {
                    String s = splitedText[i].split("<td")[1];
                    String[] list = s.split(">");
                    String arr = "";
                    for (int j = 0; j < list.length; j++) {
                        if (j == list.length - 1) {
                            arr += list[j];
                        } else if (j > 0) {
                            arr += list[j] + ">";
                        }

                    }
                    tempData += arr;

                }
            } else if (splitedText[i].contains("<tr")) {
                rowData = new TableRow(context);

                if (splitedText[i].contains("<td")) {
                    String s = splitedText[i].split("<td")[1];
                    String[] list = s.split(">");
                    String arr = "";
                    for (int j = 0; j < list.length; j++) {
                        if (j == list.length - 1) {
                            arr += list[j];
                        } else if (j > 0) {
                            arr += list[j] + ">";
                        }

                    }
                    tempData += arr;

                }

            } else if (splitedText[i].contains("<td")) {
                String s = splitedText[i].split("<td")[1];
                String[] list = s.split(">");
                String arr = "";
                for (int j = 0; j < list.length; j++) {
                    if (j == list.length - 1) {
                        arr += list[j];
                    } else if (j > 0) {
                        arr += list[j] + ">";
                    } else {

                    }

                }
                tempData += arr;

            } else if (key.startsWith("مصرف در بارداری") && (splitedText[i].startsWith("<strong"))) {
                String s = splitedText[i];
                tempData += s;
                mode = "bold";
            } else {
                mode = "p";
            }

        }

        Log.d("\n F:", "finish");
    }

    private class ViewHolder {
        TextView title;
        LinearLayout description;
        LinearLayout r1;
    }

}