package com.hamrahelm.iranpharma;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.hamrahelm.iranpharma.database.DrugCategoriesDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pedi on 8/13/2019 AD.
 */

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    public static String FRAGMENT_INDEX = "FRAGMENT_INDEX";

    public ViewHolder vh;
    public int selectedFragment = -1;

    public HomeFragment homeFragment;
    public DrugFragment drugFragment;
    public ProfileFragment profileFragment;
    public Fragment currentFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Tools.setCustomFont(this.getApplicationContext());
        hiddenKeyboard();
        initViewHolder();
        setSelectedFragment(getIntent().getIntExtra(FRAGMENT_INDEX, 0));
        // ATTENTION: This was auto-generated to handle app links.
        Intent appLinkIntent = getIntent();
        String appLinkAction = appLinkIntent.getAction();
        Uri appLinkData = appLinkIntent.getData();
    }

    @Override
    protected void onResume() {
//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        super.onResume();
    }

    @Override
    public void onBackPressed() {
        tellFragments();
    }

    private void tellFragments(){
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        for(Fragment f : fragments){
            if(f != null && f instanceof HomeFragment) {
                if (((HomeFragment) f).onBackPressed()) {
                    super.onBackPressed();
                }
            }
            if(f != null && f instanceof DrugFragment) {
                if (((DrugFragment) f).onBackPressed()) {
                    super.onBackPressed();
                }
            }
            if(f != null && f instanceof ProfileFragment) {
                if (((ProfileFragment) f).onBackPressed()) {
                    super.onBackPressed();
                }
            }
        }
    }

    public synchronized void setSelectedFragment(int selectedFragment) {
        if (this.selectedFragment == selectedFragment)
            return;

        Fragment fragment = null;
        boolean firstTimeLoading = false;
        switch (selectedFragment) {
            case 0:
                if (homeFragment == null) {
                    homeFragment = new HomeFragment();
                    firstTimeLoading = true;
                }
                fragment = homeFragment;
                break;
            case 1:
                if (drugFragment == null) {
                    drugFragment = new DrugFragment();
                    firstTimeLoading = true;
                }
                fragment = drugFragment;
                break;
            case 2:
                if (profileFragment == null) {
                    profileFragment = new ProfileFragment();
                    firstTimeLoading = true;
                }
                fragment = profileFragment;
                break;

        }

        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        if (currentFragment != null)
            transaction.detach(currentFragment);
        if (firstTimeLoading)
            transaction.replace(R.id.frame, fragment);
        else
            transaction.attach(fragment);
        transaction.commitNow();

        changeTabBar(selectedFragment);
        this.selectedFragment = selectedFragment;
        currentFragment = fragment;
    }

    public void changeTabBar(int selected) {
        List<ImageView> tabsIcons = new ArrayList<>();
        tabsIcons.add(vh.homeIcon);
        tabsIcons.add(vh.categoriesIcon);
        tabsIcons.add(vh.profileIcon);

        List<String> tabsSelectedTitle = new ArrayList<>();
        tabsSelectedTitle.add(getString(R.string.home));
        tabsSelectedTitle.add(getString(R.string.drugs));
        tabsSelectedTitle.add(getString(R.string.profile));

        List<Integer> tabsSelectedList = new ArrayList<>();
        tabsSelectedList.add(R.drawable.home);
        tabsSelectedList.add(R.drawable.search);
        tabsSelectedList.add(R.drawable.profile);

        List<Integer> tabsNotSelectedList = new ArrayList<>();
        tabsNotSelectedList.add(R.drawable.home);
        tabsNotSelectedList.add(R.drawable.search);
        tabsNotSelectedList.add(R.drawable.profile);

        for (int i = 0; i < tabsIcons.size(); i++) {
            if (i == selected) {
                tabsIcons.get(i).setImageResource(tabsNotSelectedList.get(i));
                tabsIcons.get(i).setBackgroundColor(getResources().getColor(R.color.colorPrimaryLight));
            } else {
                tabsIcons.get(i).setImageResource(tabsSelectedList.get(i));
                tabsIcons.get(i).setBackgroundColor(getResources().getColor(R.color.white));
            }
        }
    }

    private void hiddenKeyboard() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        if (getCurrentFocus() != null)
            inputMethodManager.hideSoftInputFromInputMethod(getCurrentFocus().getApplicationWindowToken(), 0);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.home) {
            hiddenKeyboard();
            setSelectedFragment(0);
        }

        if (view.getId() == R.id.categories) {
            hiddenKeyboard();
            setSelectedFragment(1);
        }

        if (view.getId() == R.id.profile) {
            hiddenKeyboard();
            setSelectedFragment(2);
        }
    }

    public void initViewHolder() {
        vh = new ViewHolder();

        vh.frame = (FrameLayout) findViewById(R.id.frame);
        vh.home = (RelativeLayout) findViewById(R.id.home);
        vh.categories = (RelativeLayout) findViewById(R.id.categories);
        vh.profile = (RelativeLayout) findViewById(R.id.profile);
        vh.homeIcon = (ImageView) findViewById(R.id.homeIcon);
        vh.categoriesIcon = (ImageView) findViewById(R.id.categoriesIcon);
        vh.profileIcon = (ImageView) findViewById(R.id.profileIcon);

        vh.profile.setOnClickListener(this);
        vh.home.setOnClickListener(this);
        vh.categories.setOnClickListener(this);

    }

    public class ViewHolder {
        FrameLayout frame;
        RelativeLayout home;
        RelativeLayout categories;
        RelativeLayout profile;
        ImageView homeIcon;
        ImageView categoriesIcon;
        ImageView profileIcon;
    }

}
