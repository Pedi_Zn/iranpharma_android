package com.hamrahelm.iranpharma;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.hamrahelm.iranpharma.adapter.LibraryAdapter;
import com.hamrahelm.iranpharma.model.Library;
import com.hamrahelm.iranpharma.model.Login;
import com.hamrahelm.iranpharma.model.Logout;
import com.hamrahelm.iranpharma.model.Profile;
import com.hamrahelm.iranpharma.model.Register;
import com.hamrahelm.iranpharma.model.Verification;
import com.hamrahelm.iranpharma.network.GetDataService;
import com.hamrahelm.iranpharma.network.RetrofitClientInstance;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Pedi on 8/13/2019 AD.
 */

public class ProfileFragment extends Fragment implements OnBackPressed {
    public static int STATE = 0;
    public static final int PROFILE_LOGIN = 1;
    public static final int LOGIN = 2;
    public static final int REGISTER = 3;
    public static final int PROFILE = 4;
    public static final int LOGOUT = 5;
    public static final int ABOUT_US = 6;
    public static final int CONTACT_US = 7;
    public static final int TERMS = 8;
    public static final int EDIT_PROFILE = 9;
    public static final int CHANGE_PASSWORD = 10;
    public static final int FORGOT_PASSWORD = 11;
    public static double bookWidth;
    public static double bookHeight;
    private PrefManager prefManager;
    boolean doubleBackToExitPressedOnce = false;

    View root;
    ViewHolder vh;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (root == null) {
            root = inflater.inflate(R.layout.fragment_profile, container, false);
            prefManager = new PrefManager(getContext());
            refreshData();
        }
        return root;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onResume() {
        super.onResume();
        initViewHolder(root);
        checkNetwork();
    }

    @Override
    public boolean onBackPressed() {
        if (STATE == PROFILE_LOGIN || STATE == PROFILE) {
            if (doubleBackToExitPressedOnce) {
                return true;
            }

            this.doubleBackToExitPressedOnce = true;
            Tools.displayToast(getContext(), "برای خروج، مجدداً کلیک کنید");

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce=false;
                }
            }, 2000);
        }
        else {
            if (STATE == EDIT_PROFILE || STATE == CHANGE_PASSWORD) {
                showProgressBar();
                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<Profile> call2 = service.profile(prefManager.getAccessToken());
                call2.enqueue(new Callback<Profile>() {
                    @Override
                    public void onResponse(Call<Profile> call, Response<Profile> response) {
                        hideProgressBar();
                        initProfile(getView(), response);
                    }

                    @Override
                    public void onFailure(Call<Profile> call, Throwable t) {
                        Tools.displayErrorToast(getContext(), getResources().getString(R.string.error));
                        hideProgressBar();
                    }
                });
                return false;
            }
            else {
                initProfileLogin(getView());
                return false;
            }
        }
        return false;
    }

    public void refreshData() {
    }

    public void initProfileLogin(final View view) {
        checkNetwork();

        STATE = PROFILE_LOGIN;

        vh.title.setText("ورود");
        hideProgressBar();

        vh.profile_login_card.setVisibility(View.VISIBLE);
        vh.aboutus_card.setVisibility(View.VISIBLE);
        vh.login_card.setVisibility(View.GONE);
        vh.register_card.setVisibility(View.GONE);
        vh.forgot_password_card.setVisibility(View.GONE);
        vh.profile_card.setVisibility(View.GONE);
        vh.purchase_card.setVisibility(View.GONE);
        vh.edit_profile_card.setVisibility(View.GONE);
        vh.change_password_card.setVisibility(View.GONE);
        vh.contact_us_card.setVisibility(View.GONE);
        vh.about_us_card.setVisibility(View.GONE);
        vh.back.setVisibility(View.GONE);
        vh.version.setVisibility(View.VISIBLE);
        vh.copy_right.setVisibility(View.VISIBLE);

        Tools.setBoldFont(vh.title);
        Tools.setFont(vh.signin);
        Tools.setFont(vh.signup);

        Tools.setFont(vh.about_us);
        Tools.setFont(vh.contact_us);
        Tools.setFont(vh.terms);

        Tools.setBoldFont(vh.version);
        Tools.setBoldFont(vh.copyright_text);
        Tools.setBoldFont(vh.booket);

        vh.version.setText("نسخه " + Tools.convertNumbersToPersian(getResources().getString(R.string.version)));

        vh.signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initLogin(view);
            }
        });

        vh.signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initRegister(view);
            }
        });

        vh.about_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initAboutUs(view);
            }
        });

        vh.contact_us.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initContactUs(view);
            }
        });

        vh.terms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initTerms(view);
            }
        });

        vh.booket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://mybooket.com"));
                startActivity(browserIntent);
            }
        });

    }

    public void initLogin(View view) {
        checkNetwork();

        STATE = LOGIN;

        vh.title.setText("ورود");
        hideProgressBar();

        vh.profile_login_card.setVisibility(View.GONE);
        vh.aboutus_card.setVisibility(View.GONE);
        vh.login_card.setVisibility(View.VISIBLE);
        vh.register_card.setVisibility(View.GONE);
        vh.forgot_password_card.setVisibility(View.GONE);
        vh.profile_card.setVisibility(View.GONE);
        vh.purchase_card.setVisibility(View.GONE);
        vh.edit_profile_card.setVisibility(View.GONE);
        vh.change_password_card.setVisibility(View.GONE);
        vh.contact_us_card.setVisibility(View.GONE);
        vh.about_us_card.setVisibility(View.GONE);
        vh.back.setVisibility(View.VISIBLE);
        vh.version.setVisibility(View.GONE);
        vh.copy_right.setVisibility(View.GONE);

        Tools.setFont(vh.l_username);
        Tools.setFont(vh.l_password);
        Tools.setFont(vh.l_login);
        Tools.setFont(vh.l_register);

        vh.l_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if (TextUtils.isEmpty(vh.l_username.getText())) {
                    Tools.displayErrorToast(getContext(), "وارد کردن شماره موبایل ضروری است");
                } else if (TextUtils.isEmpty(vh.l_password.getText())) {
                    Tools.displayErrorToast(getContext(), "وارد کردن کلمه‌عبور ضروری است");
                } else {
                    showProgressBar();
                    GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                    Call<Login> call = service.login("98" + getStandardPhone(vh.l_username.getText().toString().trim()), vh.l_password.getText().toString());
                    call.enqueue(new Callback<Login>() {
                        @Override
                        public void onResponse(Call<Login> call, Response<Login> response) {
                            hideProgressBar();
                            if (response.code() == 401) {
                                Tools.displayErrorToast(getContext(), "شماره موبایل یا کلمه‌عبور اشتباه می‌باشد");
                            } else if (response.body() != null) {
                                prefManager.setLogin(true);
                                prefManager.setAccessToken("Bearer " + response.body().getAccessToken());
                                prefManager.setKey(response.body().getKey());

                                showProgressBar();
                                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                                Call<Profile> call2 = service.profile(prefManager.getAccessToken());
                                call2.enqueue(new Callback<Profile>() {
                                    @Override
                                    public void onResponse(Call<Profile> call, Response<Profile> response) {
                                        hideProgressBar();
                                        initProfile(view, response);
                                    }

                                    @Override
                                    public void onFailure(Call<Profile> call, Throwable t) {
                                        Tools.displayErrorToast(getContext(), getResources().getString(R.string.error));
                                        hideProgressBar();
                                    }
                                });
                            }
                        }

                        @Override
                        public void onFailure(Call<Login> call, Throwable t) {
                            hideProgressBar();
                            Tools.displayErrorToast(getContext(), getResources().getString(R.string.error));
                        }
                    });
                }
            }
        });

        vh.l_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                initForgotPassword(view);
            }
        });

        vh.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initProfileLogin(view);
            }
        });
    }

    public void initRegister(View view) {
        checkNetwork();

        STATE = REGISTER;

        vh.title.setText("ثبت نام");
        hideProgressBar();

        vh.profile_login_card.setVisibility(View.GONE);
        vh.aboutus_card.setVisibility(View.GONE);
        vh.login_card.setVisibility(View.GONE);
        vh.register_card.setVisibility(View.VISIBLE);
        vh.forgot_password_card.setVisibility(View.GONE);
        vh.profile_card.setVisibility(View.GONE);
        vh.edit_profile_card.setVisibility(View.GONE);
        vh.change_password_card.setVisibility(View.GONE);
        vh.purchase_card.setVisibility(View.GONE);
        vh.contact_us_card.setVisibility(View.GONE);
        vh.about_us_card.setVisibility(View.GONE);
        vh.back.setVisibility(View.VISIBLE);
        vh.version.setVisibility(View.GONE);
        vh.copy_right.setVisibility(View.GONE);

        Tools.setFont(vh.r_fullname);
        Tools.setFont(vh.r_phone);
        Tools.setFont(vh.r_password);
        Tools.setFont(vh.r_repassword);
        Tools.setFont(vh.r_register);

        vh.r_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if (checkEmpty(vh.r_fullname.getText().toString().trim(),
                        vh.r_phone.getText().toString(),
                        vh.r_password.getText().toString(),
                        vh.r_repassword.getText().toString())) {
                    if (checkPassword(vh.r_password.getText().toString(), vh.r_repassword.getText().toString())) {
                        showProgressBar();
                        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                        Call<Verification> call = service.verification("98" + getStandardPhone(vh.r_phone.getText().toString().trim()));
                        call.enqueue(new Callback<Verification>() {
                            @Override
                            public void onResponse(Call<Verification> call, final Response<Verification> response) {
                                final Dialog dialog;
                                dialog = new Dialog(getActivity());
                                dialog.setContentView(R.layout.dialog_verification);
                                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                                RelativeLayout r1 = dialog.findViewById(R.id.r1);
                                TextView text = dialog.findViewById(R.id.text);
                                final EditText verification_code = dialog.findViewById(R.id.verification_code);
                                MaterialButton done = dialog.findViewById(R.id.done);

                                Tools.setFont(text);
                                Tools.setFont(verification_code);
                                Tools.setFont(done);

                                DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
                                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) r1.getLayoutParams();
                                params.width = displayMetrics.widthPixels*4/5;

                                dialog.show();

                                done.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(final View view) {
                                        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                                        Call<Register> call2 = service.register(
                                                response.body().getTrackingCode()
                                                , verification_code.getText().toString()
                                                , vh.r_fullname.getText().toString()
                                                , "98" + getStandardPhone(vh.r_phone.getText().toString().trim())
                                                , vh.r_password.getText().toString());
                                        call2.enqueue(new Callback<Register>() {
                                            @Override
                                            public void onResponse(Call<Register> call, Response<Register> response) {
                                                if (response.code() == 200) {
                                                    prefManager.setLogin(true);
                                                    prefManager.setAccessToken("Bearer " + response.body().getAccessToken());
                                                    prefManager.setKey(response.body().getKey());

                                                    dialog.dismiss();
                                                    vh.title.setText(R.string.profile);
                                                    GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                                                    Call<Profile> call3 = service.profile(prefManager.getAccessToken());
                                                    call3.enqueue(new Callback<Profile>() {
                                                        @Override
                                                        public void onResponse(Call<Profile> call, Response<Profile> response) {
                                                            if (response.code() == 403)
                                                                initProfileLogin(view);
                                                            else
                                                                initProfile(view, response);
                                                        }

                                                        @Override
                                                        public void onFailure(Call<Profile> call, Throwable t) {
                                                            hideProgressBar();
                                                            Tools.displayErrorToast(getContext(), getResources().getString(R.string.error));
                                                        }
                                                    });
                                                }
                                                if (response.code() == 400) {
                                                    Tools.displayErrorToast(getContext(), "کد نادرست می‌باشد");
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<Register> call, Throwable t) {
                                                hideProgressBar();
                                                Tools.displayErrorToast(getContext(), getResources().getString(R.string.error));
                                            }
                                        });
                                    }
                                });
                            }

                            @Override
                            public void onFailure(Call<Verification> call, Throwable t) {
                                hideProgressBar();
                                Tools.displayErrorToast(getContext(), getResources().getString(R.string.error));
                            }
                        });

                    }
                }
            }
        });

        vh.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initProfileLogin(view);
            }
        });
    }

    public void initForgotPassword(View view) {
        checkNetwork();

        STATE = FORGOT_PASSWORD;

        vh.title.setText("فراموشی کلمه‌عبور");
        hideProgressBar();

        vh.profile_login_card.setVisibility(View.GONE);
        vh.aboutus_card.setVisibility(View.GONE);
        vh.login_card.setVisibility(View.GONE);
        vh.register_card.setVisibility(View.GONE);
        vh.forgot_password_card.setVisibility(View.VISIBLE);
        vh.profile_card.setVisibility(View.GONE);
        vh.edit_profile_card.setVisibility(View.GONE);
        vh.change_password_card.setVisibility(View.GONE);
        vh.purchase_card.setVisibility(View.GONE);
        vh.contact_us_card.setVisibility(View.GONE);
        vh.about_us_card.setVisibility(View.GONE);
        vh.back.setVisibility(View.VISIBLE);
        vh.version.setVisibility(View.GONE);
        vh.copy_right.setVisibility(View.GONE);

        Tools.setFont(vh.f_phone);
        Tools.setFont(vh.f_password);
        Tools.setFont(vh.f_forgot_password);

        vh.f_forgot_password.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showProgressBar();
                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<Verification> call = service.verification("98" + getStandardPhone(vh.f_phone.getText().toString().trim()));
                call.enqueue(new Callback<Verification>() {
                    @Override
                    public void onResponse(Call<Verification> call, final Response<Verification> response) {
                        final Dialog dialog;
                        dialog = new Dialog(getActivity());
                        dialog.setContentView(R.layout.dialog_verification);
                        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                        RelativeLayout r1 = dialog.findViewById(R.id.r1);
                        TextView text = dialog.findViewById(R.id.text);
                        final EditText verification_code = dialog.findViewById(R.id.verification_code);
                        MaterialButton done = dialog.findViewById(R.id.done);

                        Tools.setFont(text);
                        Tools.setFont(verification_code);
                        Tools.setFont(done);

                        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
                        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) r1.getLayoutParams();
                        params.width = displayMetrics.widthPixels*4/5;

                        dialog.show();
                        done.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(final View view) {
                                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                                Call<Register> call2 = service.forgotPassword(
                                        response.body().getTrackingCode()
                                        , verification_code.getText().toString()
                                        , vh.f_password.getText().toString());
                                call2.enqueue(new Callback<Register>() {
                                    @Override
                                    public void onResponse(Call<Register> call, Response<Register> response) {
                                        if (response.code() == 200) {
                                            prefManager.setLogin(true);
                                            prefManager.setAccessToken("Bearer " + response.body().getAccessToken());
                                            prefManager.setKey(response.body().getKey());

                                            dialog.dismiss();
                                            vh.title.setText(R.string.profile);
                                            GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                                            Call<Profile> call3 = service.profile(prefManager.getAccessToken());
                                            call3.enqueue(new Callback<Profile>() {
                                                @Override
                                                public void onResponse(Call<Profile> call, Response<Profile> response) {
                                                    if (response.code() == 403)
                                                        initProfileLogin(view);
                                                    else
                                                        initProfile(view, response);
                                                }

                                                @Override
                                                public void onFailure(Call<Profile> call, Throwable t) {
                                                    hideProgressBar();
                                                    Tools.displayErrorToast(getContext(), getResources().getString(R.string.error));
                                                }
                                            });
                                        }
                                        if (response.code() == 400) {
                                            Tools.displayErrorToast(getContext(), "کد نادرست می‌باشد");
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<Register> call, Throwable t) {
                                        hideProgressBar();
                                        Tools.displayErrorToast(getContext(), getResources().getString(R.string.error));
                                    }
                                });
                            }
                        });
                    }

                    @Override
                    public void onFailure(Call<Verification> call, Throwable t) {
                        hideProgressBar();
                        Tools.displayErrorToast(getContext(), getResources().getString(R.string.error));
                    }
                });
            }
        });

        vh.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initProfileLogin(view);
            }
        });
    }

    public void initProfile(View view, final Response<Profile> response) {
        checkNetwork();

        STATE = PROFILE;

        vh.title.setText("پروفایل");
        hideProgressBar();

        vh.profile_login_card.setVisibility(View.GONE);
        vh.aboutus_card.setVisibility(View.GONE);
        vh.login_card.setVisibility(View.GONE);
        vh.register_card.setVisibility(View.GONE);
        vh.forgot_password_card.setVisibility(View.GONE);
        vh.profile_card.setVisibility(View.VISIBLE);
        vh.edit_profile_card.setVisibility(View.GONE);
        vh.change_password_card.setVisibility(View.GONE);
        vh.purchase_card.setVisibility(View.VISIBLE);
        vh.contact_us_card.setVisibility(View.GONE);
        vh.about_us_card.setVisibility(View.GONE);
        vh.back.setVisibility(View.GONE);
        vh.version.setVisibility(View.GONE);
        vh.copy_right.setVisibility(View.GONE);

        Tools.setBoldFont(vh.fullname);
        Tools.setBoldFont(vh.editprofile);
        Tools.setBoldFont(vh.changePassword);
        Tools.setBoldFont(vh.signout);

        if (response.body() != null)
            vh.fullname.setText(response.body().getTitle());

        showProgressBar();
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<List<Library>> call = service.library(prefManager.getAccessToken());
        call.enqueue(new Callback<List<Library>>() {
            @Override
            public void onResponse(Call<List<Library>> call, final Response<List<Library>> response) {
                hideProgressBar();
                if (response.code() == 200) {
                    if (response.body() != null) {
                        List<Library> libraryList = new ArrayList<>();
                        for (int i = 0; i < response.body().size(); i++) {
                            if (!response.body().get(i).getModel().equals("header")) {
                                libraryList.add(response.body().get(i));
                            }
                        }

                        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
                        bookWidth = displayMetrics.widthPixels / 2.5;
                        bookHeight = bookWidth + 3 * dpToPx(10) + 200;

                        ViewGroup.LayoutParams layoutParams = vh.purchase_card.getLayoutParams();
                        layoutParams.height = (int) (Math.ceil(((float) libraryList.size() / 2)) * bookHeight);
                        vh.purchase_card.setLayoutParams(layoutParams);

                        vh.gridView.setAdapter(new LibraryAdapter(getContext(), libraryList));
                        vh.gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                ((MainActivity) getContext()).setSelectedFragment(1);
                            }
                        });
                    }
                }
            }

            @Override
            public void onFailure(Call<List<Library>> call, Throwable t) {
                vh.title.setText(R.string.drugs);
                hideProgressBar();
                Tools.displayErrorToast(getContext(), getResources().getString(R.string.error));
            }
        });

        vh.signout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                initLogout(view);
            }
        });

        vh.editprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initEditProfile(view, response);
            }
        });

        vh.changePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initChangePassword(view, response);
            }
        });
    }

    public void initChangePassword(View view, final Response<Profile> response) {
        checkNetwork();

        STATE = CHANGE_PASSWORD;

        vh.title.setText(getResources().getString(R.string.change_password));
        hideProgressBar();

        vh.profile_login_card.setVisibility(View.GONE);
        vh.aboutus_card.setVisibility(View.GONE);
        vh.login_card.setVisibility(View.GONE);
        vh.register_card.setVisibility(View.GONE);
        vh.forgot_password_card.setVisibility(View.GONE);
        vh.profile_card.setVisibility(View.GONE);
        vh.purchase_card.setVisibility(View.GONE);
        vh.change_password_card.setVisibility(View.VISIBLE);
        vh.edit_profile_card.setVisibility(View.GONE);
        vh.contact_us_card.setVisibility(View.GONE);
        vh.about_us_card.setVisibility(View.GONE);
        vh.back.setVisibility(View.VISIBLE);
        vh.version.setVisibility(View.GONE);
        vh.copy_right.setVisibility(View.GONE);

        Tools.setFont(vh.c_old_password);
        Tools.setFont(vh.cc_old_password);
        Tools.setFont(vh.c_new_password);
        Tools.setFont(vh.c_new_repassword);
        Tools.setFont(vh.c_editprofile);

        vh.c_editprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkPassword(vh.c_new_password.getText().toString(), vh.c_new_repassword.getText().toString())) {
                    showProgressBar();
                    GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                    Call<Register> call2 = service.changePassword(prefManager.getAccessToken(), vh.c_old_password.getText().toString(), vh.c_new_password.getText().toString());
                    call2.enqueue(new Callback<Register>() {
                        @Override
                        public void onResponse(Call<Register> call, Response<Register> registerResponse) {
                            hideProgressBar();
                            if (response.body() != null) {
                                prefManager.setAccessToken("Bearer " + registerResponse.body().getAccessToken());
                                prefManager.setKey(registerResponse.body().getKey());

                                Tools.displayToast(getContext(), "کلمه‌عبور شما با موفقیت تغییر یافت");
                                initProfile(getView(), response);
                            }
                        }

                        @Override
                        public void onFailure(Call<Register> call, Throwable t) {
                            Tools.displayErrorToast(getContext(), getResources().getString(R.string.error));
                            hideProgressBar();
                        }
                    });
                }
            }
        });

        vh.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initProfile(view, response);
            }
        });

    }

    public void initEditProfile(View view, final Response<Profile> response) {
        checkNetwork();

        STATE = EDIT_PROFILE;

        vh.title.setText(getResources().getString(R.string.editprofile));
        hideProgressBar();

        vh.profile_login_card.setVisibility(View.GONE);
        vh.aboutus_card.setVisibility(View.GONE);
        vh.login_card.setVisibility(View.GONE);
        vh.register_card.setVisibility(View.GONE);
        vh.forgot_password_card.setVisibility(View.GONE);
        vh.profile_card.setVisibility(View.GONE);
        vh.purchase_card.setVisibility(View.GONE);
        vh.edit_profile_card.setVisibility(View.VISIBLE);
        vh.change_password_card.setVisibility(View.GONE);
        vh.contact_us_card.setVisibility(View.GONE);
        vh.about_us_card.setVisibility(View.GONE);
        vh.back.setVisibility(View.VISIBLE);
        vh.version.setVisibility(View.GONE);
        vh.copy_right.setVisibility(View.GONE);

        Tools.setFont(vh.e_firstname);
        Tools.setFont(vh.e_lastname);
        Tools.setFont(vh.e_email);
        Tools.setFont(vh.e_editprofile);

        if (response.body() != null) {
            vh.e_email.setText(response.body().getEmail());
            vh.e_firstname.setText(response.body().getFirstname());
            vh.e_lastname.setText(response.body().getLastname());
        }

        vh.e_editprofile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showProgressBar();
                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<Profile> call2 = service.editProfile(prefManager.getAccessToken(), vh.e_firstname.getText().toString(), vh.e_lastname.getText().toString(), vh.e_email.getText().toString());
                call2.enqueue(new Callback<Profile>() {
                    @Override
                    public void onResponse(Call<Profile> call, Response<Profile> registerResponse) {
                        hideProgressBar();
                        Tools.displayToast(getContext(), "اطلاعات شما با موفقیت تغییر یافت");
                        initProfile(getView(), registerResponse);
                    }

                    @Override
                    public void onFailure(Call<Profile> call, Throwable t) {
                        Tools.displayErrorToast(getContext(), getResources().getString(R.string.error));
                        hideProgressBar();
                    }
                });
            }
        });

        vh.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initProfile(view, response);
            }
        });

    }

    public void initLogout(final View view) {
        checkNetwork();

        STATE = LOGOUT;

        showProgressBar();
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<Logout> call = service.logout(prefManager.getAccessToken());
        call.enqueue(new Callback<Logout>() {
            @Override
            public void onResponse(Call<Logout> call, Response<Logout> response) {
                hideProgressBar();
                prefManager.setAccessToken(null);
                prefManager.setKey(null);
                prefManager.setLogin(false);
                initProfileLogin(view);
            }

            @Override
            public void onFailure(Call<Logout> call, Throwable t) {
                hideProgressBar();
                Tools.displayErrorToast(getContext(), getResources().getString(R.string.error));
            }
        });
//        final AlertDialog dialog;
//        dialog = new AlertDialog.Builder(getActivity()).create();
//        dialog.show();
//        dialog.setContentView(R.layout.dialog_logout);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//
//        TextView t2;
//        MaterialButton logout;
//
//        t2 = dialog.findViewById(R.id.t2);
//        logout = dialog.findViewById(R.id.logout);
//
//        Tools.setBoldFont(t2);
//        Tools.setBoldFont(logout);
//
//        logout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(final View view) {
//
//            }
//        });
    }

    public void initAboutUs(final View view) {
        checkNetwork();

        STATE = ABOUT_US;

        vh.title.setText(R.string.about_us);
        hideProgressBar();

        vh.profile_login_card.setVisibility(View.GONE);
        vh.aboutus_card.setVisibility(View.GONE);
        vh.login_card.setVisibility(View.GONE);
        vh.register_card.setVisibility(View.GONE);
        vh.forgot_password_card.setVisibility(View.GONE);
        vh.profile_card.setVisibility(View.GONE);
        vh.purchase_card.setVisibility(View.GONE);
        vh.edit_profile_card.setVisibility(View.GONE);
        vh.change_password_card.setVisibility(View.GONE);
        vh.contact_us_card.setVisibility(View.GONE);
        vh.about_us_card.setVisibility(View.VISIBLE);
        vh.back.setVisibility(View.VISIBLE);
        vh.version.setVisibility(View.GONE);
        vh.copy_right.setVisibility(View.GONE);

        Tools.setFont(vh.about_us_text);

        vh.about_us_text.setText(Html.fromHtml(getResources().getString(R.string.about_us_text)));

        vh.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initProfileLogin(view);
            }
        });
    }

    public void initContactUs(final View view) {
        checkNetwork();

        STATE = CONTACT_US;

        vh.title.setText(R.string.contact_us);
        vh.profile_login_card.setVisibility(View.GONE);
        vh.aboutus_card.setVisibility(View.GONE);
        vh.login_card.setVisibility(View.GONE);
        vh.register_card.setVisibility(View.GONE);
        vh.forgot_password_card.setVisibility(View.GONE);
        vh.profile_card.setVisibility(View.GONE);
        vh.purchase_card.setVisibility(View.GONE);
        vh.edit_profile_card.setVisibility(View.GONE);
        vh.change_password_card.setVisibility(View.GONE);
        vh.contact_us_card.setVisibility(View.VISIBLE);
        vh.about_us_card.setVisibility(View.GONE);
        vh.back.setVisibility(View.VISIBLE);
        vh.version.setVisibility(View.GONE);
        vh.copy_right.setVisibility(View.GONE);

        Tools.setFont(vh.contact_us_text);

        vh.contact_us_text.setText(Html.fromHtml(getResources().getString(R.string.contact_us_text)));

        vh.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initProfileLogin(view);
            }
        });
    }

    public void initTerms(final View view) {
        checkNetwork();

        STATE = TERMS;

        vh.title.setText(R.string.terms);
        vh.profile_login_card.setVisibility(View.GONE);
        vh.aboutus_card.setVisibility(View.GONE);
        vh.login_card.setVisibility(View.GONE);
        vh.register_card.setVisibility(View.GONE);
        vh.forgot_password_card.setVisibility(View.GONE);
        vh.profile_card.setVisibility(View.GONE);
        vh.purchase_card.setVisibility(View.GONE);
        vh.edit_profile_card.setVisibility(View.GONE);
        vh.change_password_card.setVisibility(View.GONE);
        vh.contact_us_card.setVisibility(View.GONE);
        vh.about_us_card.setVisibility(View.GONE);
        vh.back.setVisibility(View.VISIBLE);
        vh.version.setVisibility(View.GONE);
        vh.copy_right.setVisibility(View.GONE);

        vh.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initProfileLogin(view);
            }
        });
    }

    public void initViewHolder(final View view) {
        vh = new ViewHolder();

        vh.internet_connection = view.findViewById(R.id.internet_connection);
        vh.internetMessage = view.findViewById(R.id.internetMessage);
        vh.tryAgain = view.findViewById(R.id.tryAgain);

        vh.title = view.findViewById(R.id.title);
        vh.version = view.findViewById(R.id.version);
        vh.booket = view.findViewById(R.id.booket);
        vh.copyright_text = view.findViewById(R.id.copyright_text);
        vh.copy_right = view.findViewById(R.id.copyright);
        vh.back = view.findViewById(R.id.back);
        vh.shadow = view.findViewById(R.id.shadow);
        vh.progressbar = view.findViewById(R.id.progressbar);

        vh.signin = view.findViewById(R.id.signin);
        vh.signup = view.findViewById(R.id.signup);
        vh.about_us = view.findViewById(R.id.aboutus);
        vh.about_us_text = view.findViewById(R.id.about_us);
        vh.contact_us = view.findViewById(R.id.contactus);
        vh.contact_us_text = view.findViewById(R.id.contact_us);
        vh.terms = view.findViewById(R.id.terms);

        vh.profile_login_card = view.findViewById(R.id.profile_login_card);
        vh.aboutus_card = view.findViewById(R.id.aboutus_card);
        vh.login_card = view.findViewById(R.id.login_card);
        vh.register_card = view.findViewById(R.id.register_card);
        vh.forgot_password_card = view.findViewById(R.id.forgot_password_card);
        vh.profile_card = view.findViewById(R.id.profile_card);
        vh.edit_profile_card = view.findViewById(R.id.edit_profile_card);
        vh.change_password_card = view.findViewById(R.id.change_password_card);
        vh.purchase_card = view.findViewById(R.id.purchase_card);
        vh.contact_us_card = view.findViewById(R.id.contact_us_card);
        vh.about_us_card = view.findViewById(R.id.about_us_card);

        vh.fullname = view.findViewById(R.id.fullname);
        vh.editprofile = view.findViewById(R.id.edit_profile);
        vh.changePassword = view.findViewById(R.id.change_password);
        vh.signout = view.findViewById(R.id.signout);

        vh.l_username = view.findViewById(R.id.l_username);
        vh.l_password = view.findViewById(R.id.l_password);
        vh.l_login = view.findViewById(R.id.l_login);
        vh.l_register = view.findViewById(R.id.l_register);

        vh.r_fullname = view.findViewById(R.id.r_fullname);
        vh.r_phone = view.findViewById(R.id.r_phone);
        vh.r_password = view.findViewById(R.id.r_password);
        vh.r_repassword = view.findViewById(R.id.r_repassword);
        vh.r_register = view.findViewById(R.id.r_register);

        vh.f_phone = view.findViewById(R.id.f_phone);
        vh.f_password = view.findViewById(R.id.f_password);
        vh.f_forgot_password = view.findViewById(R.id.f_forgot_password);

        vh.e_firstname = view.findViewById(R.id.e_firstname);
        vh.e_lastname = view.findViewById(R.id.e_lastname);
        vh.e_email = view.findViewById(R.id.e_email);
        vh.e_editprofile = view.findViewById(R.id.e_editprofile);

        vh.cc_old_password = view.findViewById(R.id.cc_old_password);
        vh.c_old_password = view.findViewById(R.id.c_old_password);
        vh.c_new_password = view.findViewById(R.id.c_new_password);
        vh.c_new_repassword = view.findViewById(R.id.c_new_repassword);
        vh.c_editprofile = view.findViewById(R.id.c_editprofile);

        vh.gridView= view.findViewById(R.id.gridView);

        vh.shadow.setVisibility(View.VISIBLE);
        vh.progressbar.setVisibility(View.INVISIBLE);

        vh.gridView.setFocusable(false);

        Tools.setBoldFont(vh.title);
        Tools.setFont(vh.internetMessage);
        Tools.setFont(vh.tryAgain);

        if (!prefManager.isLoggedin()) {
            initProfileLogin(view);
        } else {
            showProgressBar();
            vh.title.setText(R.string.profile);
            GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
            Call<Profile> call = service.profile(prefManager.getAccessToken());
            call.enqueue(new Callback<Profile>() {
                @Override
                public void onResponse(Call<Profile> call, Response<Profile> response) {
                    hideProgressBar();
                    if (response.code() == 403)
                        initProfileLogin(view);
                    else
                        initProfile(view, response);
                }

                @Override
                public void onFailure(Call<Profile> call, Throwable t) {
                    hideProgressBar();
                    Tools.displayErrorToast(getContext(), getResources().getString(R.string.error));
                }
            });
        }
    }

    public boolean checkEmpty(String fullname, String phone, String password, String repassword) {
        if (TextUtils.isEmpty(phone)) {
            Tools.displayErrorToast(getContext(), "وارد کردن شماره موبایل ضروری است");
            return false;
        }
        if (TextUtils.isEmpty(fullname)) {
            Tools.displayErrorToast(getContext(), "وارد کردن نام ضروری است");
            return false;
        }
        if (TextUtils.isEmpty(password)) {
            Tools.displayErrorToast(getContext(), "وارد کردن کلمه عبور ضروری است");
            return false;
        }
        if (TextUtils.isEmpty(repassword)) {
            Tools.displayErrorToast(getContext(), "تکرار کردن کلمه عبور ضروری است");
            return false;
        }
        return true;
    }

    public boolean checkPassword(String password, String repassword) {
        if (password.length() < 6) {
            Tools.displayErrorToast(getContext(), "کلمه عبور باید حداقل از ۶ حرف تشکیل شده باشد");
            return false;
        }
        if (!password.equals(repassword)) {
            Tools.displayErrorToast(getContext(), "کلمه عبور با تکرار آن همخوانی ندارد");
            return false;
        }
        return true;
    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public String getStandardPhone(String phone) {
        if (phone == null)
            return "";

        phone = Tools.convertNumbersToEnglish(phone);
        String phoneNumber = phone.replace(" ", "").trim();
        phoneNumber = phoneNumber.replace("-", "").trim();

        if (phoneNumber.startsWith("0"))
            phoneNumber = phoneNumber.substring(1);
        if (phoneNumber.startsWith("98"))
            phoneNumber = phoneNumber.substring(2);
        if (phoneNumber.startsWith("+98"))
            phoneNumber = phoneNumber.substring(3);

        return phoneNumber;
    }

    public void showProgressBar() {
        vh.shadow.setVisibility(View.INVISIBLE);
        vh.progressbar.setVisibility(View.VISIBLE);
    }

    public void hideProgressBar() {
        vh.shadow.setVisibility(View.VISIBLE);
        vh.progressbar.setVisibility(View.INVISIBLE);
    }

    private void checkNetwork() {
        if (Tools.isConnected(getContext())) {
            vh.internet_connection.setVisibility(View.GONE);
        } else {
            vh.internet_connection.setVisibility(View.VISIBLE);
            vh.tryAgain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onResume();
                }
            });
        }
    }

    public static class ViewHolder {
        RelativeLayout internet_connection;
        TextView internetMessage;
        MaterialButton tryAgain;

        TextView title;
        TextView version;
        TextView booket;
        TextView copyright_text;
        LinearLayout copy_right;
        ImageView back;
        View shadow;
        ProgressBar progressbar;

        TextView t2;
        MaterialButton logout;

        FrameLayout profile_login_card;
        FrameLayout aboutus_card;
        FrameLayout login_card;
        FrameLayout register_card;
        FrameLayout forgot_password_card;
        FrameLayout edit_profile_card;
        FrameLayout change_password_card;
        FrameLayout profile_card;
        FrameLayout purchase_card;
        FrameLayout contact_us_card;
        FrameLayout about_us_card;

        TextInputEditText l_username;
        TextInputEditText l_password;
        MaterialButton l_login;
        MaterialButton l_register;

        TextInputEditText r_fullname;
        TextInputEditText r_phone;
        TextInputEditText r_password;
        TextInputEditText r_repassword;
        MaterialButton r_register;

        TextInputEditText f_phone;
        TextInputEditText f_password;
        MaterialButton f_forgot_password;

        TextInputEditText e_firstname;
        TextInputEditText e_lastname;
        TextInputEditText e_email;
        MaterialButton e_editprofile;

        TextInputLayout cc_old_password;
        TextInputEditText c_old_password;
        TextInputEditText c_new_password;
        TextInputEditText c_new_repassword;
        MaterialButton c_editprofile;

        GridView gridView;

        TextView signin;
        TextView signup;

        TextView about_us;
        TextView about_us_text;
        TextView contact_us;
        TextView contact_us_text;
        TextView terms;

        TextView fullname;
        TextView editprofile;
        TextView changePassword;
        TextView signout;
    }
}
