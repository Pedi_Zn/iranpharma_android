package com.hamrahelm.iranpharma.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.SectionIndexer;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.hamrahelm.iranpharma.R;
import com.hamrahelm.iranpharma.Tools;
import com.hamrahelm.iranpharma.model.DrugCard;
import com.hamrahelm.iranpharma.network.RetrofitClientInstance;

import java.util.ArrayList;
import java.util.List;

public class SearchDrugAdapter extends BaseAdapter implements SectionIndexer {

    private ArrayList<Integer> mSectionPositions;
    public List<DrugCard> drugList;
    public Context context;

    public SearchDrugAdapter(Context context, List<DrugCard> drugList) {
        this.context = context;
        this.drugList = drugList;
    }

    public int getCount() {
        return drugList.size();
    }

    public DrugCard getItem(int position) {
        return drugList.get(position);
    }

    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ClickableViewAccessibility")
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder vh;

        if (convertView != null) {
            vh = (ViewHolder) convertView.getTag();
        } else {
            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.list_item, parent, false);

            vh = new ViewHolder();
            vh.title = (TextView) convertView.findViewById(R.id.title);
            vh.image = convertView.findViewById(R.id.image);
            vh.image.setPadding(10,10,10,10);

            vh.image.setVisibility(View.VISIBLE);

            Tools.setFont(vh.title);

            convertView.setTag(vh);
        }
        DrugCard drugCard = getItem(position);
        if (drugCard != null) {
            vh.title.setText(drugCard.getTitle());

            Glide.with(context)
                    .load(RetrofitClientInstance.BASE_URL + drugList.get(position).getIcon())
                    .apply(new RequestOptions()
                            .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                            .diskCacheStrategy(DiskCacheStrategy.NONE))
                    .transition(DrawableTransitionOptions.withCrossFade(150))
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    })
                    .into(vh.image);
        }

        return convertView;
    }

    @Override
    public Object[] getSections() {
        List<String> sections = new ArrayList<>();
        mSectionPositions = new ArrayList<>();
        for (int i = 0, size = drugList.size(); i < size; i++) {
            String section = String.valueOf(drugList.get(i).getTitle().charAt(0)).toUpperCase();
            if (!sections.contains(section)) {
                sections.add(section);
                mSectionPositions.add(i);
            }
        }
        return sections.toArray(new String[0]);
    }

    @Override
    public int getPositionForSection(int i) {
        return mSectionPositions.get(i);
    }

    @Override
    public int getSectionForPosition(int i) {
        return 0;
    }

    private class ViewHolder {
        TextView title;
        ImageView image;
    }

}
