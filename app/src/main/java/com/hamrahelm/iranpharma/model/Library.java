package com.hamrahelm.iranpharma.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Library {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("color")
    @Expose
    private String color;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("icon")
    @Expose
    private String icon;
    @SerializedName("icon_library")
    @Expose
    private String iconLibrary;
    @SerializedName("jdate_expire_at")
    @Expose
    private String jdate_expire_at;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getIconLibrary() {
        return iconLibrary;
    }

    public void setIconLibrary(String iconLibrary) {
        this.iconLibrary = iconLibrary;
    }

    public String getJdate_expire_at() {
        return jdate_expire_at;
    }

    public void setJdate_expire_at(String jdate_expire_at) {
        this.jdate_expire_at = jdate_expire_at;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }
}