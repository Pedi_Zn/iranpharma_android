package com.hamrahelm.iranpharma.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.hamrahelm.iranpharma.R;
import com.hamrahelm.iranpharma.Tools;
import com.hamrahelm.iranpharma.model.DrugCard;

import java.util.ArrayList;
import java.util.List;

public class DrugAdapter extends BaseAdapter implements SectionIndexer {

    private ArrayList<Integer> mSectionPositions;
    public List<DrugCard> drugList;
    public Context context;

    public DrugAdapter(Context context, List<DrugCard> drugList) {
        this.context = context;
        this.drugList = drugList;
    }

    public int getCount() {
        return drugList.size();
    }

    public DrugCard getItem(int position) {
        return drugList.get(position);
    }

    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ClickableViewAccessibility")
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder vh;

        if (convertView != null) {
            vh = (ViewHolder) convertView.getTag();
        } else {
            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.list_item, parent, false);

            vh = new ViewHolder();
            vh.title = (TextView) convertView.findViewById(R.id.title);

            Tools.setFont(vh.title);

            convertView.setTag(vh);
        }
        DrugCard drugCard = getItem(position);
        if (drugCard != null) {
            vh.title.setText(drugCard.getTitle());
        }

        return convertView;
    }

    @Override
    public Object[] getSections() {
        List<String> sections = new ArrayList<>();
        mSectionPositions = new ArrayList<>();
        for (int i = 0, size = drugList.size(); i < size; i++) {
            String section = String.valueOf(drugList.get(i).getTitle().charAt(0)).toUpperCase();
            if (!sections.contains(section)) {
                sections.add(section);
                mSectionPositions.add(i);
            }
        }
        return sections.toArray(new String[0]);
    }

    @Override
    public int getPositionForSection(int i) {
        return mSectionPositions.get(i);
    }

    @Override
    public int getSectionForPosition(int i) {
        return 0;
    }

    private class ViewHolder {
        TextView title;
    }

}
