package com.hamrahelm.iranpharma;

import android.animation.TimeInterpolator;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toolbar;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.hamrahelm.iranpharma.adapter.VitrineAdapter;
import com.hamrahelm.iranpharma.model.Book;
import com.hamrahelm.iranpharma.model.Version;
import com.hamrahelm.iranpharma.model.Vitrine;
import com.hamrahelm.iranpharma.network.GetDataService;
import com.hamrahelm.iranpharma.network.RetrofitClientInstance;
import com.google.android.material.button.MaterialButton;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Pedi on 8/13/2019 AD.
 */

public class HomeFragment extends Fragment implements OnBackPressed {
    private int MARGINE_SIZE = 60;
    private int TITLE_SIZE = 170;
    public static boolean firstTime = true;

    private List<Vitrine> vitrineList;
    private double sum;
    public static double bookWidth;
    public static double bookHeight;
    private PrefManager prefManager;
    boolean doubleBackToExitPressedOnce = false;

    View root;
    ViewHolder vh;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefManager = new PrefManager(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (root == null) {
            root = inflater.inflate(R.layout.fragment_home, container, false);
//            initViewHolder(root);
            refreshData();
        }
        return root;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void refreshData() {

    }

    @Override
    public void onResume() {
        super.onResume();
        initViewHolder(root);
        checkNetwork();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public boolean onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            return true;
        }

        this.doubleBackToExitPressedOnce = true;
        Tools.displayToast(getContext(), "برای خروج، مجدداً کلیک کنید");

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
        return false;
    }

    public void initViewHolder(View view) {
        vh = new ViewHolder();

        TITLE_SIZE = Tools.getScreenHeight(getContext()) / 10;

        vh.progressbar = view.findViewById(R.id.progressbar);
        vh.internet_connection = view.findViewById(R.id.internet_connection);
        vh.internetMessage = view.findViewById(R.id.internetMessage);
        vh.tryAgain = view.findViewById(R.id.tryAgain);
        vh.vitrine = view.findViewById(R.id.vitrine);
        vh.back = view.findViewById(R.id.back);
        vh.title = view.findViewById(R.id.title);
        vh.bookList = view.findViewById(R.id.bookList);

        Tools.setFont(vh.internetMessage);
        Tools.setFont(vh.tryAgain);

        Tools.setBoldFont(vh.title);

        vh.vitrine.setVisibility(View.GONE);
        vh.back.setVisibility(View.GONE);

        vh.title.setText(R.string.home);

        DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
        bookWidth = displayMetrics.widthPixels / 3.5;
        bookHeight = (int) (Math.sqrt(2) * bookWidth) + 60;

        showProgressBar();
        if (Tools.isConnected(getContext())) {
            GetDataService service0 = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
            Call<Version> call0 = service0.version();
            call0.enqueue(new Callback<Version>() {
                @Override
                public void onResponse(Call<Version> call0, final Response<Version> response) {
                    if (response.body() != null) {
                        if (!response.body().getVersion().equals(getResources().getString(R.string.version)) && firstTime) {
                            if (response.body().getModel().equals("soft")) {
                                final Dialog dialog;
                                dialog = new Dialog(getActivity());
                                dialog.setContentView(R.layout.dialog_update);
                                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                                RelativeLayout r1 = dialog.findViewById(R.id.r1);
                                TextView nuText = dialog.findViewById(R.id.nuText);
                                MaterialButton nuOk = dialog.findViewById(R.id.nuOk);
                                MaterialButton nuCancel = dialog.findViewById(R.id.nuCancel);

                                Tools.setFont(nuText);
                                Tools.setFont(nuOk);
                                Tools.setFont(nuCancel);

                                nuText.setText(getContext().getResources().getString(R.string.need_update));
                                firstTime = false;

                                DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
                                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) r1.getLayoutParams();
                                params.width = displayMetrics.widthPixels * 4 / 5;

                                nuOk.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Uri url = Uri.parse(response.body().getUrl());
                                        Intent intent = new Intent(Intent.ACTION_VIEW, url);
                                        startActivity(intent);
                                    }
                                });

                                nuCancel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        dialog.dismiss();
                                        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                                        Call<List<Vitrine>> call = service.vitrine(prefManager.getAccessToken());
                                        call.enqueue(new Callback<List<Vitrine>>() {
                                            @Override
                                            public void onResponse(Call<List<Vitrine>> call, Response<List<Vitrine>> response) {
                                                if (response.body() != null) {
                                                    hideProgressBar();
                                                    vh.vitrine.setVisibility(View.VISIBLE);
                                                    vitrineList = response.body();

                                                    for (int i = 0; i < vitrineList.size(); i++) {
                                                        sum += Math.ceil(((float) vitrineList.get(i).getData().size() / 3));
                                                    }
                                                    ViewGroup.LayoutParams layoutParams = vh.bookList.getLayoutParams();
                                                    layoutParams.height = (int) (sum * bookHeight) + (vitrineList.size() * TITLE_SIZE) + (MARGINE_SIZE * 3); //60: marginBottom
                                                    vh.bookList.setLayoutParams(layoutParams);
                                                    sum = 0;
                                                    vh.bookList.setAdapter(new VitrineAdapter(getContext(), vitrineList));
                                                }
                                            }

                                            @Override
                                            public void onFailure(Call<List<Vitrine>> call, Throwable t) {
                                                Tools.displayErrorToast(getContext(), getResources().getString(R.string.error));
                                            }
                                        });
                                    }
                                });

                                dialog.show();

                            }
                            if (response.body().getModel().equals("hard")) {
                                final Dialog dialog;
                                dialog = new Dialog(getActivity());
                                dialog.setContentView(R.layout.dialog_update);
                                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                                RelativeLayout r1 = dialog.findViewById(R.id.r1);
                                TextView nuText = dialog.findViewById(R.id.nuText);
                                MaterialButton nuOk = dialog.findViewById(R.id.nuOk);
                                MaterialButton nuCancel = dialog.findViewById(R.id.nuCancel);

                                Tools.setFont(nuText);
                                Tools.setFont(nuOk);
                                Tools.setFont(nuCancel);

                                nuText.setText(getContext().getResources().getString(R.string.need_force_update));
                                firstTime = false;

                                DisplayMetrics displayMetrics = getContext().getResources().getDisplayMetrics();
                                FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) r1.getLayoutParams();
                                params.width = displayMetrics.widthPixels * 4 / 5;

                                nuOk.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Uri url = Uri.parse(response.body().getUrl());
                                        Intent intent = new Intent(Intent.ACTION_VIEW, url);
                                        startActivity(intent);
                                    }
                                });

                                nuCancel.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        dialog.dismiss();
                                        getActivity().finish();
                                    }
                                });

                                dialog.show();
                            }
                        } else {
                            GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                            Call<List<Vitrine>> call = service.vitrine(prefManager.getAccessToken());
                            call.enqueue(new Callback<List<Vitrine>>() {
                                @Override
                                public void onResponse(Call<List<Vitrine>> call, Response<List<Vitrine>> response) {
                                    if (response.body() != null) {
                                        hideProgressBar();
                                        vh.vitrine.setVisibility(View.VISIBLE);
                                        vitrineList = response.body();

                                        for (int i = 0; i < vitrineList.size(); i++) {
                                            sum += Math.ceil(((float) vitrineList.get(i).getData().size() / 3));
                                        }

                                        ViewGroup.LayoutParams layoutParams = vh.bookList.getLayoutParams();
                                        layoutParams.height = (int) (sum * bookHeight) + (vitrineList.size() * TITLE_SIZE) + (MARGINE_SIZE * 3); //60: marginBottom
                                        vh.bookList.setLayoutParams(layoutParams);
                                        sum = 0;
                                        vh.bookList.setAdapter(new VitrineAdapter(getContext(), vitrineList));
                                    }
                                }

                                @Override
                                public void onFailure(Call<List<Vitrine>> call, Throwable t) {
                                    Tools.displayErrorToast(getContext(), getResources().getString(R.string.error));
                                }
                            });
                        }
                    }
                }

                @Override
                public void onFailure(Call<Version> call, Throwable t) {
                    Tools.displayErrorToast(getContext(), getResources().getString(R.string.error));
                }
            });
        }

    }

    private void checkNetwork() {
        if (Tools.isConnected(getContext())) {
            vh.internet_connection.setVisibility(View.GONE);
        } else {
            vh.internet_connection.setVisibility(View.VISIBLE);
            vh.tryAgain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onResume();
                }
            });
        }
    }

    private void showProgressBar() {
        vh.progressbar.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar() {
        vh.progressbar.setVisibility(View.GONE);
    }

    public static class ViewHolder {
        ProgressBar progressbar;

        RelativeLayout internet_connection;
        TextView internetMessage;
        MaterialButton tryAgain;

        FrameLayout vitrine;

        TextView title;
        ImageView back;

        TextView company;

        ListView bookList;
    }
}
