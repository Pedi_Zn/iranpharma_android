package com.hamrahelm.iranpharma.model;

import com.google.gson.JsonArray;
import com.hamrahelm.iranpharma.model.SubCategory;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Drug {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("model_id")
    @Expose
    private int modelId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("title_en")
    @Expose
    private String titleEn;
    @SerializedName("title_tejari")
    @Expose
    private String titleTejari;
    @SerializedName("cure_class")
    @Expose
    private String cureClass;
    @SerializedName("dosage")
    @Expose
    private String dosage;
    @SerializedName("prescribing_consumables")
    @Expose
    private String prescribingConsumables;
    @SerializedName("safety_tips")
    @Expose
    private String safetyTips;
    @SerializedName("complications")
    @Expose
    private String complications;
    @SerializedName("contraindication")
    @Expose
    private String contraindication;
    @SerializedName("attention")
    @Expose
    private String attention;
    @SerializedName("interactions")
    @Expose
    private String interactions;
    @SerializedName("pregnancy")
    @Expose
    private String pregnancy;
    @SerializedName("food_considerations")
    @Expose
    private String foodConsiderations;
    @SerializedName("monitoring_parameters")
    @Expose
    private String monitoringParameters;
    @SerializedName("reference_scope")
    @Expose
    private String referenceScope;
    @SerializedName("pharmacodynamics")
    @Expose
    private String pharmacodynamics;
    @SerializedName("additional_information")
    @Expose
    private String additionalInformation;
    @SerializedName("compound_products")
    @Expose
    private String compoundProducts;
    @SerializedName("medical_information")
    @Expose
    private String medicalInformation;
    @SerializedName("sources")
    @Expose
    private String sources;
    @SerializedName("us")
    @Expose
    private String us;
    @SerializedName("picture")
    @Expose
    private String picture;
    @SerializedName("publish")
    @Expose
    private String publish;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("sub_categories")
    @Expose
    private List<SubCategory> subCategories;
    @SerializedName("tables")
    @Expose
    private String tables;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getModelId() {
        return modelId;
    }

    public void setModelId(int modelId) {
        this.modelId = modelId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleEn() {
        return titleEn;
    }

    public void setTitleEn(String titleEn) {
        this.titleEn = titleEn;
    }

    public String getTitleTejari() {
        return titleTejari;
    }

    public void setTitleTejari(String titleTejari) {
        this.titleTejari = titleTejari;
    }

    public String getCureClass() {
        return cureClass;
    }

    public void setCureClass(String cureClass) {
        this.cureClass = cureClass;
    }

    public String getDosage() {
        return dosage;
    }

    public void setDosage(String dosage) {
        this.dosage = dosage;
    }

    public String getPrescribingConsumables() {
        return prescribingConsumables;
    }

    public void setPrescribingConsumables(String prescribingConsumables) {
        this.prescribingConsumables = prescribingConsumables;
    }

    public String getSafetyTips() {
        return safetyTips;
    }

    public void setSafetyTips(String safetyTips) {
        this.safetyTips = safetyTips;
    }

    public String getComplications() {
        return complications;
    }

    public void setComplications(String complications) {
        this.complications = complications;
    }

    public String getContraindication() {
        return contraindication;
    }

    public void setContraindication(String contraindication) {
        this.contraindication = contraindication;
    }

    public String getAttention() {
        return attention;
    }

    public void setAttention(String attention) {
        this.attention = attention;
    }

    public String getInteractions() {
        return interactions;
    }

    public void setInteractions(String interactions) {
        this.interactions = interactions;
    }

    public String getPregnancy() {
        return pregnancy;
    }

    public void setPregnancy(String pregnancy) {
        this.pregnancy = pregnancy;
    }

    public String getFoodConsiderations() {
        return foodConsiderations;
    }

    public void setFoodConsiderations(String foodConsiderations) {
        this.foodConsiderations = foodConsiderations;
    }

    public String getMonitoringParameters() {
        return monitoringParameters;
    }

    public void setMonitoringParameters(String monitoringParameters) {
        this.monitoringParameters = monitoringParameters;
    }

    public String getReferenceScope() {
        return referenceScope;
    }

    public void setReferenceScope(String referenceScope) {
        this.referenceScope = referenceScope;
    }

    public String getPharmacodynamics() {
        return pharmacodynamics;
    }

    public void setPharmacodynamics(String pharmacodynamics) {
        this.pharmacodynamics = pharmacodynamics;
    }

    public String getAdditionalInformation() {
        return additionalInformation;
    }

    public void setAdditionalInformation(String additionalInformation) {
        this.additionalInformation = additionalInformation;
    }

    public String getCompoundProducts() {
        return compoundProducts;
    }

    public void setCompoundProducts(String compoundProducts) {
        this.compoundProducts = compoundProducts;
    }

    public String getMedicalInformation() {
        return medicalInformation;
    }

    public void setMedicalInformation(String medicalInformation) {
        this.medicalInformation = medicalInformation;
    }

    public String getSources() {
        return sources;
    }

    public void setSources(String sources) {
        this.sources = sources;
    }

    public String getUs() {
        return us;
    }

    public void setUs(String us) {
        this.us = us;
    }

    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getPublish() {
        return publish;
    }

    public void setPublish(String publish) {
        this.publish = publish;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public List<SubCategory> getSubCategories() {
        return subCategories;
    }

    public void setSubCategories(List<SubCategory> subCategories) {
        this.subCategories = subCategories;
    }

    public String getTables() {
        return tables;
    }

    public void setTables(String tables) {
        this.tables = tables;
    }
}
