package com.hamrahelm.iranpharma.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Handler;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.WebView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import com.hamrahelm.iranpharma.DrugsPage;
import com.hamrahelm.iranpharma.R;
import com.hamrahelm.iranpharma.TableActivity;
import com.hamrahelm.iranpharma.Tools;
import com.hamrahelm.iranpharma.model.Drug;
import com.hamrahelm.iranpharma.network.RetrofitClientInstance;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Pedi & Sabi on 01-01-2020.
 */

public class DrugListAdapter2 extends BaseAdapter {
    private Animation animationUp;
    private Animation animationDown;
    public ArrayList<HashMap<String, String>> drugList;
    public int screenHeight;
    public Context context;
    public ListView listView;
    public int drugSize;
    public Drug drug;
    public ArrayList<Integer> cardFieldsStat = new ArrayList();
    public HashMap<String, ArrayList<String>> tableUrl = new HashMap<String, ArrayList<String>>();
    public HashMap<String, String> drugsName = new HashMap<>();

    public DrugListAdapter2(Context context, ArrayList<HashMap<String, String>> drugList, ListView listView, int drugSize, HashMap<String, ArrayList<String>> tableUrl, Drug drug) {
        this.context = context;
        this.drugList = drugList;
        this.listView = listView;
        this.drugSize = drugSize;
        this.tableUrl = tableUrl;
        this.drug = drug;
    }

    public int getCount() {
        return drugList.size();
    }

    public HashMap<String, String> getItem(int position) {
        return drugList.get(position);
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        final DrugListAdapter2.ViewHolder vh;

        if (convertView != null) {
            vh = null;// (DrugListAdapter2.ViewHolder) convertView.getTag();
        } else {
            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.list_item_drug, parent, false);
            HashMap<String, String> drugCard = getItem(position);
            String key = "", value = "";
            if (drugCard != null) {
                if (drugCard.keySet().toString().startsWith("[") && drugCard.keySet().toString().endsWith("]")) {
                    key = drugCard.keySet().toString().substring(1, drugCard.keySet().toString().length() - 1);
                }
                if (drugCard.values().toString().startsWith("[") && drugCard.values().toString().endsWith("]")) {
                    value = drugCard.values().toString().substring(1, drugCard.values().toString().length() - 1);
                }
                screenHeight = context.getResources().getDisplayMetrics().heightPixels / 11;

                vh = convertHtmltoXml(key, /*context.getResources().getString(R.string.text1)*/ value);

                vh.title.setText(key);
//            vh.description.setText(key);
//            vh.description.setText(Html.fromHtml(value));
//                final TextView s = (TextView) (vh.description.getChildAt(0));

                if (key.equals(context.getString(R.string.sources))) {
                    TextView textView = new TextView(context);
                    textView.setText(Html.fromHtml("<p class=\"ql-align-right\" >" + value + "</p>"));
                    textView.setTextColor(context.getResources().getColor(R.color.colorPrimary));
                    Tools.setFont(textView);
                    vh.description.removeAllViews();
                    vh.description.addView(textView);
                }
                cardFieldsStat.add(0);

                vh.title.setId(R.id.title);
                vh.description.setOrientation(LinearLayout.VERTICAL);
                vh.description.setId(R.id.description);
                vh.description.removeAllViews();

                String[] splitedValue = value.split("<khar");
                ArrayList<WebView> splited2 = new ArrayList<>();
                ArrayList<WebView> splitedAfter = new ArrayList<>();
                ArrayList<HorizontalScrollView> tablesList = new ArrayList<>();
                if(splitedValue.length>0){
                    for(int z = 0 ;z < splitedValue.length ; z++) {
                        if(z>0) {
                            Log.d("", String.valueOf(splitedValue.length));
                            String jkj = splitedValue[z].split("</table>")[0];
                            jkj = "<table" + jkj + "</table>";
                            HorizontalScrollView sc = convertTable(jkj);
                            sc.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                            tablesList.add(sc);
                            String badeTable = splitedValue[z].split("</table>")[1];

                            final WebView  vw = new WebView(context);
                            vw.clearCache(true);
                            vw.clearHistory();
                            vw.getSettings().setJavaScriptEnabled(true);
                            vw.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
                            String htmlData = "<html><link rel=\"stylesheet\" type=\"text/css\" href=\"css/style.css\" />"+
                                    "<style type=\"text/css\">@font-face { font-family: myFont; src: url(\"file:///assets/fonts/iransansmobile_light.ttf\"); } body,p,h1,h2,h3,h4,h5,h6,div,span,strong,table,td,tr,tbody,footer,header, {font-family: myFont !important;} tr{background-color:black !important;}  </style>"
                                    + "<body class=\"ql-editor ql-align-right\" style=\"direction:rtl;font-family:myFont;\">"+badeTable+"</body><html><script>" +
                                    "let j = document.querySelectorAll(\"a\");\n" +
                                    "for(var mj in j ){\n" +
                                    "if(j[mj] && (typeof j[mj] === \"object\")){\n" +
                                    "\tj[mj].addEventListener(\"click\",function(e){\n" +
                                    "\t\te.preventDefault();\n" +
                                    "\t\te.preventDefaults();\n" +
                                    "\t});\n" +
                                    "\t}\n" +
                                    "}"+
                                    "</style>";


                            // lets assume we have /assets/style.css file
                            htmlData = htmlData.replaceAll("#ec008c","#364FB3");
                            vw.loadDataWithBaseURL("file:///assets/", htmlData, "text/html", "UTF-8", null);
                            Tools.setFont(vw);
                            splitedAfter.add(vw);

                        }
                        else{

                            final WebView  vw = new WebView(context);
                            vw.clearCache(true);
                            vw.clearHistory();
                            vw.getSettings().setJavaScriptEnabled(true);
                            vw.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
                            String htmlText = splitedValue[z];
                            htmlText.replaceAll("<span>","<span><div id='ann2'></div>");
                            String htmlData = "<html><link rel=\"stylesheet\" type=\"text/css\" href=\"css/style.css\" />"+
                                    "<style type=\"text/css\">" +
                                    "@font-face { font-family: myFont; src: url(\"file:///assets/fonts/iransansmobile_light.ttf\"); } body,p,h1,h2,h3,h4,h5,h6,div,span,strong,table,td,tr,tbody,footer,header, {font-family: myFont !important;}  " +
//                                    "body{}"+
                                    "body{\" -webkit-user-select: none !important;\" +}"+
                                    "table{ display:block; width:100% !important;overflow-x:auto;overflow-y:hidden;justify-content: center !important;}" +
                                    "tbody{display:block; width:100% !important;}" +
                                    "p{display:inline !important;}" +
                                    "td{ padding:20px 5px !important;max-width:1200px;max-height:500px;min-height:180px !important;}" +
                                    "td span{-webkit-user-select: none;\" +\n" +
                                    "-webkit-touch-callout: none;" +
                                    "-moz-user-select: none;" +
                                    "-ms-user-select: none;" +
                                    "user-select: none;" +
                                    "float:right;text-align:right;text-align:center;margin-right:2px !important;}" +
                                    "p span{  -webkit-user-select: none;" +
                                    "-webkit-touch-callout: none;" +
                                    "-moz-user-select: none;" +
                                    "-ms-user-select: none;" +
                                    "user-select: none;" +
                                    "white-space: nowrap !important ; margin : 0.5px 1.5px  !important ; display:inline-block; word-break:break-all !important ;}" +
                                    "td > p{min-width:150px;display:inline-block;float:right;text-align:right;clear:both !important;padding:0 12px !important;}" +
                                    "p, span, h1, h2, h3, h4, h5, h5, h6, div, a, td, tr {" +
                                    "-webkit-touch-callout: none !important;" +
                                    "-webkit-user-select: none !important;" +
                                    "-khtml-user-select: none !important;" +
                                    "-moz-user-select: none !important;" +
                                    "-ms-user-select: none !important;" +
                                    "user-select: none !important;" +
                                    "}" +
                                    "tr{background-color:#E9F3FF;padding:10px 7px !important;text-align:center !important;}"+
                                    "tr:nth-child(1){\n" +
                                    "padding:15px 5px !important; "+
                                    "\tbackground-color:#364FB3 !important;\n" +
                                    "color:white !important;"+
                                    "}\n" +
                                    "span{word-break: break-all !important; white-space: nowrap; !important;  margin: 0.4px 1.4px !important; " +
                                    "position:relative;display:inline-block;}"+
                                    " </style>"
                                    + "<body class=\"ql-editor ql-align-right\" style=\"direction:rtl;font-family:myFont;\">" +
                                    ""+ htmlText +"<div id='ann'></div></body><html><script>" +
                                    "let j = document.querySelectorAll(\"a\");\n" +
                                    "for(var mj in j ){\n" +
                                    "if(j[mj] && (typeof j[mj] === \"object\")){\n" +
                                    "\tj[mj].addEventListener(\"click\",function(e){\n" +
                                    "\t\te.preventDefault();\n" +
                                    "\t\te.preventDefaults();\n" +
                                    "\t});\n" +
                                    "\t}\n" +
                                    "}"+
                                    "var touching = null;\n" +
                                    "$('span').each(function() {\n" +
                                    "    this.addEventListener(\"touchstart\", function(e) {\n" +
                                    "        e.preventDefault();\n" +
                                    "        touching = window.setTimeout(longTouch, 300, true);\n" +
                                    "    }, false);\n" +
                                    "    this.addEventListener(\"touchend\", function(e) {\n" +
                                    "        e.preventDefault();\n" +
                                    "        window.clearTimeout(touching);\n" +
                                    "    }, false);\n" +
                                    "});\n" +
                                    "\n" +
                                    "</script>";

                                    if((key.equals("اشکال دارويى"))){
                                        String temp = htmlData.split("</style>")[0];
                                        temp = temp + "span{text-align:left !important;float:left !important;}"+
                                                        "p{text-align:left !important;float:left;}";
                                        htmlData = temp + "</style>" + htmlData.split("</style>")[1];
                                    }
                                    // lets assume we have /assets/style.css file
                                    htmlData = htmlData.replaceAll("#ec008c","#364FB3");
                                    htmlData = htmlData.replaceAll("undefined","");
                                    vw.loadDataWithBaseURL("file:///assets/", htmlData, "text/html", "UTF-8", null);
                                    Tools.setFont(vw);
                                    vw.setClickable(false);

                                    splited2.add(vw);
                        }
                    }
                }


                for(int tt=0 ; tt < splitedValue.length;tt++){
                    if((tt< splited2.size()) && splited2.get(tt)!=null){
                        vh.description.addView(splited2.get(tt));
                    }
                    if((tt< tablesList.size()) &&  tablesList.get(tt)!=null){
                        vh.description.addView(tablesList.get(tt));
                    }
                    if((tt< splitedAfter.size()) &&  splitedAfter.get(tt)!=null){
                        vh.description.addView(splitedAfter.get(tt));
                    }

                }

//                Tools.setFont(vh.description);
                vh.r1 = convertView.findViewById(R.id.r1);
                vh.description.setVerticalGravity(Gravity.TOP);

                animationUp = AnimationUtils.loadAnimation(context, R.anim.slide_up);
                animationDown = AnimationUtils.loadAnimation(context, R.anim.slide_down);

                Tools.setFont(vh.title);
                Tools.setFont(vh.description);

                vh.title.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                    if (vh.description.isShown()) {
                        vh.title.setCompoundDrawablesWithIntrinsicBounds(R.drawable.bottom, 0, 0, 0);
                        vh.description.startAnimation(animationUp);
                        vh.description.setVisibility(View.GONE);
                        vh.linear.setVisibility(View.GONE);
                        cardFieldsStat.set(position, 0);
                        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) listView.getLayoutParams();
                        int sumHeight = 0;
                        for (int i = 0; i < cardFieldsStat.size(); i++) {
                            sumHeight += cardFieldsStat.get(i) ;
                        }
//                            params.height = screenHeight * drugSize; //
                        params.height = (screenHeight * drugSize) + sumHeight;
                        Log.d("sum of heigh  : ", String.valueOf(params.height ));

                        listView.setLayoutParams(params);
                    }
                    else {
                        vh.title.setCompoundDrawablesWithIntrinsicBounds(R.drawable.top, 0, 0, 0);
                        vh.description.setVisibility(View.VISIBLE);
                        vh.linear.setVisibility(View.VISIBLE);
                        vh.description.startAnimation(animationDown);

//                            Tools.displayToast(context, String.valueOf(vh.description.getHeight()));
//                            Tools.displayErrorToast(context, String.valueOf(vh.description.getY()));
//                            vh.r1.post(new Runnable() {
//                                public void run() {
                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
//                                            Tools.displayErrorToast(context, String.valueOf(vh.description.getHeight()));
                                        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) listView.getLayoutParams();
                                        if (tableUrl.get(TransName(vh.title.getText().toString())) != null) {
                                            cardFieldsStat.set(position, (vh.description.getHeight() +  vh.linear.getHeight() + vh.linear.getPaddingBottom() + vh.linear.getPaddingTop()));
                                        }
                                        else {
                                            cardFieldsStat.set(position, (vh.description.getHeight()));
                                        }
                                        int sumHeight = 0;
                                        for (int i = 0; i < cardFieldsStat.size(); i++) {
                                            sumHeight = sumHeight + cardFieldsStat.get(i);
                                            Log.d("height  : " + i + "  ", String.valueOf(cardFieldsStat.get(i)));
                                        }
//                                            int sumOfChildren = 0;
                                        params.height = (screenHeight * drugSize) + sumHeight;
//                                            vh.divider.getVerticalScrollbarPosition();
                                        Log.d("sum of height  : ", String.valueOf(params.height));
                                        listView.setLayoutParams(params);
                                    }
                                }, 300);
//                                }
//                            });
                    }
                    }
                });
//
            } else {
                vh = null;
            }

        }

        if (vh != null) {
            final LinearLayout l = convertView.findViewById(R.id.r1);
            l.removeAllViews();
            vh.title.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, screenHeight));
            vh.title.setCompoundDrawablesWithIntrinsicBounds(R.drawable.bottom, 0, 0, 0);
            vh.title.setGravity(Gravity.RIGHT | Gravity.CENTER);
            vh.title.setPadding(Tools.dpToPx(context, 10), Tools.dpToPx(context, 1), Tools.dpToPx(context, 10), Tools.dpToPx(context, 1));
            vh.title.setTextColor(context.getResources().getColor(R.color.gray_dark));
            vh.title.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.text_size_p3));
            l.addView(vh.title);

            LinearLayout l2 = new LinearLayout(context);
            l2.setPadding(10, 10, 70, 10);
            l2.setHorizontalGravity(1);
            l2.setGravity(Gravity.TOP | Gravity.RIGHT);

            vh.description.setGravity(Gravity.RIGHT | Gravity.CENTER);
            vh.description.setPadding(Tools.dpToPx(context, 10), Tools.dpToPx(context, 1), Tools.dpToPx(context, 10), Tools.dpToPx(context, 1));

            if (tableUrl.get(TransName(vh.title.getText().toString())) != null) {
                Button btn  = new Button(context);
                final ArrayList urlList = new ArrayList();
                for (int i = 0; i < tableUrl.get(TransName(vh.title.getText().toString())).size(); i++) {
                    urlList.add(RetrofitClientInstance.BASE_URL_STORAGE + tableUrl.get(TransName(vh.title.getText().toString())).get(i));
                }
                Tools.setFont(btn);
                btn.setText(R.string.table);

                btn.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
                btn.setTextColor(context.getResources().getColor(R.color.white));
                btn.setBackground(context.getResources().getDrawable(R.drawable.gray_button));
                btn.setTextSize(TypedValue.COMPLEX_UNIT_PX, context.getResources().getDimension(R.dimen.text_size_p2_5));
                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        l.removeAllViews();
                        Intent intent = new Intent(context, TableActivity.class);
                        intent.putExtra("urlList", urlList);
                        intent.putExtra("title", drug.getTitle());
                        context.startActivity(intent);
                    }
                });
                l2.addView(btn);
            }
            else {
            }

            vh.linear = l2;
            vh.linear.setVisibility(View.GONE);
            vh.description.setVisibility(View.GONE);
            vh.description.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

//            if ((position)  == 0) {
//                vh.linear.setVisibility(View.VISIBLE);
//                vh.description.setVisibility(View.VISIBLE);
//                vh.description.setVisibility(View.VISIBLE);
//                vh.description.setAnimation(animationDown);
//                l.post(new Runnable() {
//                    public void run() {
//                        Handler handler = new Handler();
//                        handler.postDelayed(new Runnable() {
//                            @Override
//                            public void run() {
//                                cardFieldsStat.set(position, (vh.description.getHeight()+ vh.linear.getHeight() + vh.linear.getPaddingBottom() + vh.linear.getPaddingTop()));
//                                l.getChildAt(1).setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, vh.description.getHeight()));
//                                listView.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, (listView.getHeight()) + vh.description.getHeight() + vh.linear.getHeight() + vh.linear.getPaddingBottom() + vh.linear.getPaddingTop()));
//                            }
//                        }, 1000);
//                    }
//                });
//            }
//            else {
//                vh.description.setVisibility(View.GONE);
//                vh.linear.setVisibility(View.GONE);
//                vh.description.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
//
//            }


            l.addView(vh.description);
            if (tableUrl.get(TransName(vh.title.getText().toString())) != null) {
                l.addView(l2);
            }

            ImageView view = new ImageView(context);
            vh.divider = view;
//            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) listView.getLayoutParams();
//            params.height =  4;
            vh.divider.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
//            view.setLayoutParams(params);
//            vh.divider.setPaddingRelative(0, 100, 0, 0);
            vh.divider.setMaxHeight(3);
            vh.divider.setMinimumHeight(2);
            l.addView(vh.divider);

        }

        return convertView;
    }

    public DrugListAdapter2.ViewHolder convertHtmltoXml(String key, String text) {
        TextView textView = new TextView(context);
        DrugListAdapter2.ViewHolder localVh = new ViewHolder();
        localVh.description = new LinearLayout(context);
        LinearLayout l2 = new LinearLayout(context);
        Button btn = new Button(context);
        btn.setHeight(40);
        btn.setText("salam");

        l2.addView(btn);
        localVh.title = new TextView(context);

        textView.setText(text);
        textView.setTextColor(context.getResources().getColor(R.color.white));
        return localVh;
//
    }

    public HorizontalScrollView convertTable(String text){
        String mode = "table";
        String rowString = "";
        String tempData = "";
        HorizontalScrollView hsv=null;
        int rowNumber = 0;
        TableRow rowData = new TableRow(context);
        TableLayout tempTable = new TableLayout(context);
        String[] splitedText = text.split("</");
        for (int i = 0; i < splitedText.length; i++) {
            if (splitedText[i].startsWith("table")) {
                splitedText[i] = splitedText[i].substring(6);
            }
//            else if (splitedText[i].startsWith("strong")) {
//                tempData += "</" + splitedText[i];
//                if (mode.equals("bold")) {
//                    TextView t2 = new TextView(context);
//                    Tools.setFont(t2);
//                    t2.setText(Html.fromHtml(tempData));
//                    localVh.description.removeAllViews();
//                    localVh.description.addView(t2);
//                    tempData = "";
//                    mode = "";
//                } else {
//                    splitedText[i] = splitedText[i].substring(7);
//                }
//            }

            else if (splitedText[i].startsWith("tbody")) {
                mode = "";
                rowNumber = 0;
                HorizontalScrollView horizontalScrollView = new HorizontalScrollView(context);
                horizontalScrollView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tempTable.setStretchAllColumns(true);
                tempTable.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                horizontalScrollView.addView(tempTable);
                View tempRow = tempTable.getChildAt(tempTable.getChildCount() - 1);
                tempRow.setBackground(context.getResources().getDrawable(R.drawable.row_rounded_bottom));
                horizontalScrollView.setPadding(0, 0, 0, 40);
//                localVh.description.addView(horizontalScrollView);
                hsv = horizontalScrollView;
                return hsv;
//                tempTable = new TableLayout(context);

            }
            else if (splitedText[i].startsWith("tr")) {
                splitedText[i] = splitedText[i].substring(3);
                rowData.setLayoutParams(new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT));
                if (rowNumber == 0) {
                    rowData.setBackground(context.getResources().getDrawable(R.drawable.row_rounded_top));
                }
                else {
                    rowData.setBackgroundColor(context.getResources().getColor(R.color.white));
                }
                rowData.setId(i);
                rowData.setPadding(10, 50, 10, 0);
                rowData.setHorizontalGravity(Gravity.CENTER_HORIZONTAL);
                rowData.setVerticalGravity(Gravity.CENTER_VERTICAL);
                tempTable.addView(rowData);
                rowNumber++;
                Log.d("ROW DATA : ", String.valueOf(rowData.getId()));

                rowData = new TableRow(context);
                // add row to tempTable
            }
            else if (splitedText[i].startsWith("td")) {
                TextView label_date1 = new TextView(context);
                Tools.setFont(label_date1);
                label_date1.setId(i + 1);
                tempData = tempData.replaceAll("</p>","</p>\n");
                label_date1.setText(Html.fromHtml(tempData));
                label_date1.setTextColor(Color.BLACK);
//                label_date1.setMaxWidth(800);
                label_date1.setLineSpacing(58, 0);
                label_date1.setGravity(Gravity.CENTER_HORIZONTAL);
                label_date1.setPadding(35, 1, 15, 1);
                label_date1.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
//                label_date1.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.row_rounded_top));

                if (rowNumber == 0) {
                    label_date1.setMaxLines(1);
                    label_date1.setHeight(155);
                    label_date1.setTextColor(Color.WHITE);
                    label_date1.setBackgroundColor(Color.TRANSPARENT);
                } else {
                    label_date1.setMaxLines(1);
                    label_date1.setMaxHeight(200);
                    label_date1.setTextColor(Color.BLACK);
                    label_date1.setBackgroundColor(Color.TRANSPARENT);
                }
                rowData.addView(label_date1);// add the column to the table row here
                splitedText[i] = splitedText[i].substring(3);
                rowString += tempData + "\n Z : ";
                tempData = "";

                // add Text View to Row
            }
//            else if (splitedText[i].startsWith("h3")) {
//                TextView t2 = new TextView(context);
//                Tools.setFont(t2);
//                t2.setText(Html.fromHtml(tempData + "</h3>"));
//                t2.setTextColor(context.getResources().getColor(R.color.colorPrimary));
//                localVh.description.removeAllViews();
//                localVh.description.addView(t2);
//                splitedText[i] = splitedText[i].substring(3);
//                tempData = "";
//            }
//            else if (splitedText[i].startsWith("h4")) {
//                TextView t2 = new TextView(context);
//                Tools.setFont(t2);
//                t2.setText(Html.fromHtml(tempData + "</h4>"));
//                t2.setTextColor(context.getResources().getColor(R.color.colorPrimary));
//                localVh.description.removeAllViews();
//                localVh.description.addView(t2);
//                splitedText[i] = splitedText[i].substring(3);
//                tempData = "";
//            }
//            else if (splitedText[i].startsWith("h5")) {
//                TextView t2 = new TextView(context);
//                Tools.setFont(t2);
//                t2.setText(Html.fromHtml(tempData + "</h5>"));
//                t2.setTextColor(context.getResources().getColor(R.color.colorPrimary));
//                localVh.description.removeAllViews();
//                localVh.description.addView(t2);
//                splitedText[i] = splitedText[i].substring(3);
//                tempData = "";
//            }
            else if (splitedText[i].startsWith("span")) {
                tempData += "</" + splitedText[i];
                splitedText[i] = splitedText[i].substring(5);
            }
            else {

            }

//            if (splitedText[i].startsWith("<p") || splitedText[i].startsWith("<h4") || splitedText[i].startsWith("<h5") || splitedText[i].startsWith("<h3")) {
//
//                if (!mode.equals("table")) {
//                    mode = "p";
//                } else {
//
//
//                }
//
//                tempData += splitedText[i];
//
//            }
            // else
             if (splitedText[i].contains("<tbody")) {

                tempTable = new TableLayout(context);
                mode = "table";
                rowNumber = 0;
                if (splitedText[i].contains("<tr")) {
                    rowData = new TableRow(context);
                }
                if (splitedText[i].contains("<td")) {
                    String s = splitedText[i].split("<td")[1];
                    String[] list = s.split(">");
                    String arr = "";
                    for (int j = 0; j < list.length; j++) {
                        if (j == list.length - 1) {
                            arr += list[j];
                        } else if (j > 0) {
                            arr += list[j] + ">";
                        }

                    }
                    tempData += arr;

                }
            }
            else if (splitedText[i].contains("<tr")) {
                rowData = new TableRow(context);

                if (splitedText[i].contains("<td")) {
                    String s = splitedText[i].split("<td")[1];
                    String[] list = s.split(">");
                    String[] list2 = s.split("<p");

//                    String[] list3 = s.split("");
                    String arr = "";
                    for (int j = 0; j < list.length; j++) {
                        if (j == list.length - 1) {
                            arr += list[j];
                        } else if (j > 0) {
                            arr += list[j] + ">";
                        }

                    }
                    tempData += arr;


                }

            }
            else if (splitedText[i].contains("<td")) {
                String s = splitedText[i].split("<td")[1];
                String[] list = s.split(">");
                String arr = "";
                for (int j = 0; j < list.length; j++) {
                    if (j == list.length - 1) {
                        arr += list[j];
                    } else if (j > 0) {
                        arr += list[j] + ">";
                    } else {

                    }

                }
                tempData += arr;

            }
//            else if (key.startsWith("مصرف در بارداری") && (splitedText[i].startsWith("<strong"))) {
//                String s = splitedText[i];
//                tempData += s;
//                if(!mode.equals("p"))
//                    mode = "bold";
//            }
//            else {
////                mode = "p";
//            }

        }

        Log.d("\n F:", "finish");
        return hsv;
    }

    public String TransName (String name) {
        if (name.equals(context.getResources().getString(R.string.dosage2))){
            drugsName.put(name, "dosage");
            return "dosage";
        }
        if (name.equals(context.getResources().getString(R.string.monitoring_parameters2))){
            drugsName.put(name, "monitoring_parameters");
            return "monitoring_parameters";
        }
        if (name.equals(context.getResources().getString(R.string.interactions))){
            drugsName.put(name, "interactions");
            return "interactions";
        }
        else {
            return null;
        }
    }

    private class ViewHolder {
        TextView title;
        LinearLayout linear;
        LinearLayout description;
        LinearLayout r1;
        ImageView divider;

    }

}