package com.hamrahelm.iranpharma.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.hamrahelm.iranpharma.DrugFragment;
import com.hamrahelm.iranpharma.DrugsPage;
import com.hamrahelm.iranpharma.HomeFragment;
import com.hamrahelm.iranpharma.MainActivity;
import com.hamrahelm.iranpharma.R;
import com.hamrahelm.iranpharma.StorePage;
import com.hamrahelm.iranpharma.Tools;
import com.hamrahelm.iranpharma.model.Book;
import com.hamrahelm.iranpharma.model.Vitrine;
import com.hamrahelm.iranpharma.welcome.WelcomeActivity;

import java.util.List;

public class VitrineAdapter extends BaseAdapter {

    public static int STATE = 0;
    public static final int BUY = 1;
    public static final int BUY_ALL = 2;

    public static List<Vitrine> vitrineList;
    public List<Book> bookList;
    public static Book book;
    public static int quantity;
    public static int bookId;
    public static int bookPrice;
    public static int allBooksPrice;
    public static String bookTitle;
    public static String bookModel;
    public static String bookDesc;
    public static String icon;
    public Context context;
    private VitrineAdapter.ViewHolder vh;

    public VitrineAdapter(Context context, List<Vitrine> vitrineList) {
        this.context = context;
        this.vitrineList = vitrineList;
    }

    public int getCount() {
        return vitrineList.size();
    }

    public Vitrine getItem(int position) {
        return vitrineList.get(position);
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView != null) {
            vh = (VitrineAdapter.ViewHolder) convertView.getTag();
        } else {
            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.list_item_vitrin, parent, false);

            vh = new VitrineAdapter.ViewHolder();
            vh.r1 = convertView.findViewById(R.id.r1);
            vh.title = convertView.findViewById(R.id.title);
            vh.titles = convertView.findViewById(R.id.titles);
            vh.buyAll = convertView.findViewById(R.id.buyAll);
            vh.listView = convertView.findViewById(R.id.gridView);

            Tools.setFont(vh.title);
            Tools.setFont(vh.buyAll);

            bookList = vitrineList.get(position).getData();

            ViewGroup.LayoutParams layoutParams0 = vh.titles.getLayoutParams();
            layoutParams0.height = Tools.getScreenHeight(context) / 10; //this is in pixels
            vh.titles.setLayoutParams(layoutParams0);

            ViewGroup.LayoutParams layoutParams = vh.listView.getLayoutParams();
            layoutParams.height = (int) (Math.ceil(((float) bookList.size() / 3)) * HomeFragment.bookHeight ); //this is in pixels
            vh.listView.setLayoutParams(layoutParams);

            vh.listView.setAdapter(new BookAdapter(context, vitrineList.get(position).getData()));
            vh.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    STATE = BUY;
                    book = vitrineList.get(position).getData().get(i);
                    bookId = book.getId();
                    bookPrice = book.getPrice();
                    bookDesc = book.getDescription();
                    quantity = book.getQuantity();
                    icon = book.getIcon();
                    allBooksPrice = vitrineList.get(position).getAllPrice();
                    if (vitrineList.get(position).getTitle().equals("درسنامه")) {
                        bookTitle = "درسنامه";
                        bookModel = "category";
                    }
                    if (vitrineList.get(position).getTitle().equals("دستنامه")) {
                        bookTitle = "دستنامه";
                        bookModel = "sub_category";
                    }
                    if (vitrineList.get(position).getTitle().equals("سایر")) {
                        bookTitle = "سایر";
                        bookModel = "sub_category";
                    }
//                    if (vitrineList.get(position).getData().get(i).getTitle().equals("جامع (داروهای ژنریک)")) {
//                        STATE = BUY_ALL;
//                    }

                    if (book.getHasfield()) {
                        ((MainActivity) context).setSelectedFragment(1);
                    } else {
                        Intent intent = new Intent(context, StorePage.class);
                        context.startActivity(intent);
                    }
                }
            });

            vh.buyAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    STATE = BUY_ALL;
                    allBooksPrice = vitrineList.get(position).getAllPrice();

                    if (vitrineList.get(position).getTitle().equals("درسنامه")) {
                        bookTitle = "درسنامه";
                        bookModel = "book";
                    }
                    if (vitrineList.get(position).getTitle().equals("دستنامه")) {
                        bookTitle = "دستنامه";
                        bookModel = "minibook";
                    }
                    if (vitrineList.get(position).getTitle().equals("سایر")) {
                        bookTitle = "سایر";
                        bookModel = "sayer";
                    }

                    Intent intent = new Intent(context, StorePage.class);
                    context.startActivity(intent);
                }
            });

            if (position % 2 == 0) {
                vh.r1.setBackgroundColor(context.getResources().getColor(R.color.colorPrimaryLight));
            } else {
                vh.r1.setBackgroundColor(context.getResources().getColor(R.color.white));
            }

            convertView.setTag(vh);
        }
        Vitrine vitrine = getItem(position);
        if (vitrine != null) {
            vh.title.setText(vitrine.getTitle());

            int visibility = View.VISIBLE;
            if (vitrine.getTitle().equals("سایر")) {
                visibility = View.GONE;
            }
            if (vitrine.getTitle().equals("درسنامه")) {
                int sum = 0;
                for (int j = 0; j < vitrineList.get(0).getData().size(); j++) {
                    if (vitrineList.get(0).getData().get(j).getHasfield())
                        sum++;
                }
                if (sum == vitrineList.get(0).getData().size()) {
                    visibility = View.GONE;
                } else {
                    visibility = View.VISIBLE;
                }
            }
            if (vitrine.getTitle().equals("دستنامه")) {
                int sum = 0;
                for (int j = 0; j < vitrineList.get(1).getData().size(); j++) {
                    if (vitrineList.get(1).getData().get(j).getHasfield())
                        sum++;
                }
                if (sum == vitrineList.get(1).getData().size()) {
                    visibility = View.GONE;
                } else {
                    visibility = View.VISIBLE;
                }
            }
            vh.buyAll.setVisibility(visibility);
        }


        return convertView;
    }

    private class ViewHolder {
        RelativeLayout r1;
        RelativeLayout titles;
        TextView title;
        TextView buyAll;
        GridView listView;
    }

}