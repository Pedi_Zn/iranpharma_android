package com.hamrahelm.iranpharma.adapter;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.hamrahelm.iranpharma.HomeFragment;
import com.hamrahelm.iranpharma.R;
import com.hamrahelm.iranpharma.Tools;
import com.hamrahelm.iranpharma.model.Book;
import com.hamrahelm.iranpharma.model.Vitrine;
import com.hamrahelm.iranpharma.network.RetrofitClientInstance;

import java.util.List;

public class BookAdapter extends BaseAdapter {

    public List<Book> bookList;
    public Context context;
    private BookAdapter.ViewHolder vh;

    public BookAdapter(Context context, List<Book> bookList) {
        this.context = context;
        this.bookList = bookList;
    }

    public int getCount() {
        return bookList.size();
    }

    public Book getItem(int position) {
        return bookList.get(position);
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView != null) {
            vh = (BookAdapter.ViewHolder) convertView.getTag();
        } else {
            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.list_item_book, parent, false);

            vh = new BookAdapter.ViewHolder();
            vh.status = convertView.findViewById(R.id.status);
            vh.item = convertView.findViewById(R.id.item);
            vh.imageLayout = convertView.findViewById(R.id.imageLayout);
            vh.image = convertView.findViewById(R.id.image);
            vh.statusLayout = convertView.findViewById(R.id.statusLayout);

            Tools.setFont(vh.status);

            convertView.setTag(vh);
        }
        Book book = getItem(position);
        if (book != null) {
            vh.statusLayout.setVisibility(View.VISIBLE);

            if (bookList.get(position).getHasfield()) {
                vh.status.setText("در کتابخانه");
                vh.statusLayout.setBackground(context.getDrawable(R.drawable.gray_button));
            }
            else {
                vh.status.setText(Tools.convertNumbersToPersian(book.getPrice()) + " تومان");
            }
            makeImage();
            Glide.with(context)
                    .load(RetrofitClientInstance.BASE_URL + book.getIcon())
                    .apply(new RequestOptions()
                            .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                            .diskCacheStrategy(DiskCacheStrategy.NONE))
                    .transition(DrawableTransitionOptions.withCrossFade(150))
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    })
                    .into(vh.image);
        }

        return convertView;
    }

    private void makeImage () {
        ViewGroup.LayoutParams layoutParams = vh.item.getLayoutParams();
        layoutParams.height = (int) HomeFragment.bookHeight;
        layoutParams.width = (int) HomeFragment.bookWidth;
        vh.item.setLayoutParams(layoutParams);

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) vh.imageLayout.getLayoutParams();
        params.height = (int) HomeFragment.bookWidth;
        vh.imageLayout.setLayoutParams(params);

        RelativeLayout.LayoutParams params2 = (RelativeLayout.LayoutParams) vh.statusLayout.getLayoutParams();
        params2.height = (int) HomeFragment.bookHeight - (int) HomeFragment.bookWidth + dpToPx(10) + 20;
        vh.statusLayout.setLayoutParams(params2);
    }

    public int dpToPx(int dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    private class ViewHolder {
        RelativeLayout item;
        RelativeLayout imageLayout;
        ImageView image;
        LinearLayout statusLayout;
        TextView status;
    }

}