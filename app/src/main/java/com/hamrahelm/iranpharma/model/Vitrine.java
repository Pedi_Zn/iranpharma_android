package com.hamrahelm.iranpharma.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Vitrine {
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("all_price")
    @Expose
    private Integer allPrice;
    @SerializedName("data")
    @Expose
    private List<Book> data = null;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getAllPrice() {
        return allPrice;
    }

    public void setAllPrice(Integer allPrice) {
        this.allPrice = allPrice;
    }

    public List<Book> getData() {
        return data;
    }

    public void setData(List<Book> data) {
        this.data = data;
    }
}
