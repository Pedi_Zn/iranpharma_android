package com.hamrahelm.iranpharma.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.hamrahelm.iranpharma.R;
import com.hamrahelm.iranpharma.Tools;
import com.hamrahelm.iranpharma.model.Category;
import com.hamrahelm.iranpharma.model.SubCategory;

import java.util.List;

public class SubCategoryAdapter extends BaseAdapter {

    public List<SubCategory> categoryList;
    public Context context;

    public SubCategoryAdapter(Context context, List<SubCategory> categoryList) {
        this.context = context;
        this.categoryList = categoryList;
    }

    public int getCount() {
        return categoryList.size();
    }

    public SubCategory getItem(int position) {
        return categoryList.get(position);
    }

    public long getItemId(int position) {
        return 0;
    }

    @SuppressLint("ClickableViewAccessibility")
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder vh;

        if (convertView != null) {
            vh = (ViewHolder) convertView.getTag();
        } else {
            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.list_item, parent, false);

            vh = new ViewHolder();
            vh.title = (TextView) convertView.findViewById(R.id.title);

            Tools.setFont(vh.title);

            convertView.setTag(vh);
        }
        SubCategory subCategory = getItem(position);
        if (subCategory != null) {
            vh.title.setText(subCategory.getTitle());
        }

        return convertView;
    }

    private class ViewHolder {
        TextView title;
    }

}
