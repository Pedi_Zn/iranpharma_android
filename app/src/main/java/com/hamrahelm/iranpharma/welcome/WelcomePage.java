package com.hamrahelm.iranpharma.welcome;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.hamrahelm.iranpharma.R;
import com.hamrahelm.iranpharma.MainActivity;
import com.hamrahelm.iranpharma.PrefManager;
import com.hamrahelm.iranpharma.Tools;

/**
 * Created by Pedi on 8/13/2019 AD.
 */

public class WelcomePage extends AppCompatActivity {

    private PrefManager prefManager;
    public ViewHolder vh;
    Animation animFadeIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.welcome_page);
        Tools.setCustomFont(this.getApplicationContext());
        initViewHolder();
    }

    private void initViewHolder() {
        vh = new ViewHolder();
        vh.logo = findViewById(R.id.logo);
        vh.r1 = findViewById(R.id.r1);

        checkImage();
//        convertHtmltoXml(getResources().getString(R.string.testText));

        animFadeIn = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fade_in);

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                prefManager = new PrefManager(WelcomePage.this);
                if (!prefManager.isFirstTimeLaunch()) {
                    launchHomeScreen();
                    finish();
                } else {
                    launchWelcomeActivity();
                    finish();
                }
            }
        }, 2500);
    }

    public void convertHtmltoXml(String text) {
        TextView textView = new TextView(getApplicationContext());
        textView.setText(text);
        textView.setTextColor(getApplicationContext().getResources().getColor(R.color.white));
        String mode = "p";
        String rowString = "";
        String tempData = "";
        int rowNumber = 0;
        TableRow rowData = new TableRow(getApplicationContext());
        TableLayout tempTable = new TableLayout(getApplicationContext());
        String[] splitedText = text.split("</");
        for (int i = 0; i < splitedText.length; i++) {
            if (splitedText[i].startsWith("table")) {
                splitedText[i] = splitedText[i].substring(6);
            }
            else if (splitedText[i].startsWith("strong")) {
                if(splitedText[i].contains("مصرف  تصویب  نشده")){
                    Log.d("","shit");
                }
                tempData += "</" + splitedText[i];
                if (mode.equals("bold")) {
                    TextView t2 = new TextView(getApplicationContext());
                    Tools.setFont(t2);
                    t2.setText(Html.fromHtml(tempData));

//                    vh.r1.removeAllViews();
                    vh.r1.addView(t2);
                    tempData = "";
                    mode = "";
                } else {
                    splitedText[i] = splitedText[i].substring(7);
                }


            }
            else if (splitedText[i].startsWith("p")) {
                splitedText[i] = splitedText[i].substring(2);
                if(splitedText[i].contains("مصرف  تصویب  نشده")){
                    Log.d("","shit");
                }
                if (!mode.equals("table")) {
                    TextView t2 = new TextView(getApplicationContext());
                    Tools.setFont(t2);
                    tempData = tempData + "</p>";
                    t2.setText(Html.fromHtml(tempData));
                    t2.setTextColor(Color.BLACK);
//                    vh.r1.removeAllViews();
                    vh.r1.addView(t2);
                    tempData = "";
                } else {
                    tempData += "</p>" + splitedText[i];
//                    tempData = tempData.replace("lang=", "style=\"line-height:0px;\" lang=");
                }
            }
            else if (splitedText[i].startsWith("tbody")) {
                mode = "";
                rowNumber = 0;
                HorizontalScrollView horizontalScrollView = new HorizontalScrollView(getApplicationContext());
                horizontalScrollView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                tempTable.setStretchAllColumns(true);
                tempTable.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
                horizontalScrollView.addView(tempTable);
                View tempRow = tempTable.getChildAt(tempTable.getChildCount()-1);
                tempRow.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.row_rounded_bottom));
                horizontalScrollView.setPadding(0,0,0,40);
                vh.r1.addView(horizontalScrollView);
                tempTable = new TableLayout(getApplicationContext());
                if(splitedText[i].contains("ادم  مغزی")){
                    Log.d("","shit");
                }

            }
            else if (splitedText[i].startsWith("tr")) {
                splitedText[i] = splitedText[i].substring(3);
                rowData.setLayoutParams(new ViewGroup.LayoutParams(
                        ViewGroup.LayoutParams.WRAP_CONTENT,
                        ViewGroup.LayoutParams.WRAP_CONTENT));
                if (rowNumber == 0) {
                    rowData.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.row_rounded_top));
//                    rowData.setBackgroundColor(Color.TRANSPARENT);
                }
                else {
//                    rowData.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.row_rounded_top));
                    rowData.setBackgroundColor(getApplicationContext().getResources().getColor(R.color.white));
                }
                rowData.setId(i);
                rowData.setPadding(10, 50, 10, 0);
                rowData.setHorizontalGravity(Gravity.CENTER_HORIZONTAL);
                rowData.setVerticalGravity(Gravity.CENTER_VERTICAL);
                tempTable.addView(rowData);
                rowNumber++;
                Log.d("ROW DATA : ", String.valueOf(rowData.getId()));

                rowData = new TableRow(getApplicationContext());
                // add row to tempTable
            }
            else if (splitedText[i].startsWith("td")) {
                TextView label_date1 = new TextView(getApplicationContext());
                Tools.setFont(label_date1);
                label_date1.setId(i + 1);
                label_date1.setText(Html.fromHtml(tempData));
                label_date1.setTextColor(Color.BLACK);
                label_date1.setMaxWidth(600);
                label_date1.setLineSpacing(35, 0);
                label_date1.setGravity(Gravity.CENTER_HORIZONTAL);
                label_date1.setPadding(35, 1, 15, 1);
                label_date1.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
//                label_date1.setBackground(getApplicationContext().getResources().getDrawable(R.drawable.row_rounded_top));

                if (rowNumber == 0) {
                    label_date1.setMaxLines(1);
                    label_date1.setHeight(155);
                    label_date1.setTextColor(Color.WHITE);
                    label_date1.setBackgroundColor(Color.TRANSPARENT);
                } else {
                    label_date1.setMaxLines(1);
                    label_date1.setMaxHeight(200);
                    label_date1.setTextColor(Color.BLACK);
                    label_date1.setBackgroundColor(Color.TRANSPARENT);
                }
                rowData.addView(label_date1);// add the column to the table row here
                splitedText[i] = splitedText[i].substring(3);
                rowString += tempData + "\n Z : ";
                tempData = "";

                // add Text View to Row
            }
            else if (splitedText[i].startsWith("h4")) {
                TextView t2 = new TextView(getApplicationContext());
                Tools.setFont(t2);
                t2.setText(Html.fromHtml(tempData + "</h4>"));
                t2.setTextColor(getApplicationContext().getResources().getColor(R.color.colorPrimary));
//                vh.r1.removeAllViews();
                vh.r1.addView(t2);
                splitedText[i] = splitedText[i].substring(3);
                tempData = "";
            }
            else if (splitedText[i].startsWith("h5")) {
                TextView t2 = new TextView(getApplicationContext());
                Tools.setFont(t2);
                t2.setText(Html.fromHtml(tempData + "</h5>"));
                t2.setTextColor(getApplicationContext().getResources().getColor(R.color.colorPrimary));
//                vh.r1.removeAllViews();
                vh.r1.addView(t2);
                splitedText[i] = splitedText[i].substring(3);
                tempData = "";
            }
            else if (splitedText[i].startsWith("h3")) {
                if(splitedText[i].contains("مصرف  تصویب  نشده")){
                    Log.d("","shit");
                }
                TextView t2 = new TextView(getApplicationContext());
                Tools.setFont(t2);
                t2.setText(Html.fromHtml(tempData + "</h3>"));
                t2.setTextColor(getApplicationContext().getResources().getColor(R.color.colorPrimary));
//                vh.r1.removeAllViews();
                vh.r1.addView(t2);
                splitedText[i] = splitedText[i].substring(3);
                tempData = "";
            }
            else if (splitedText[i].startsWith("span")) {
                tempData += "</" + splitedText[i];
                splitedText[i] = splitedText[i].substring(5);
            } else {

            }

            if (splitedText[i].startsWith("<p") || splitedText[i].startsWith("<h4") || splitedText[i].startsWith("<h5") || splitedText[i].startsWith("<h3")) {

                if (!mode.equals("table")) {
                    mode = "p";
                } else {


                }

                tempData += splitedText[i];

            }
            else if (splitedText[i].contains("<tbody")) {

                tempTable = new TableLayout(getApplicationContext());
                mode = "table";
                rowNumber = 0;
                if (splitedText[i].contains("<tr")) {
                    rowData = new TableRow(getApplicationContext());
                }
                if (splitedText[i].contains("<td")) {
                    String s = splitedText[i].split("<td")[1];
                    String[] list = s.split(">");
                    String arr = "";
                    for (int j = 0; j < list.length; j++) {
                        if (j == list.length - 1) {
                            arr += list[j];
                        } else if (j > 0) {
                            arr += list[j] + ">";
                        }

                    }
                    tempData += arr;

                }
            } else if (splitedText[i].contains("<tr")) {
                rowData = new TableRow(getApplicationContext());

                if (splitedText[i].contains("<td")) {
                    String s = splitedText[i].split("<td")[1];
                    String[] list = s.split(">");
                    String arr = "";
                    for (int j = 0; j < list.length; j++) {
                        if (j == list.length - 1) {
                            arr += list[j];
                        } else if (j > 0) {
                            arr += list[j] + ">";
                        }

                    }
                    tempData += arr;

                }

            } else if (splitedText[i].contains("<td")) {
                String s = splitedText[i].split("<td")[1];
                String[] list = s.split(">");
                String arr = "";
                for (int j = 0; j < list.length; j++) {
                    if (j == list.length - 1) {
                        arr += list[j];
                    } else if (j > 0) {
                        arr += list[j] + ">";
                    } else {

                    }

                }
                tempData += arr;

            } else if (/*key.startsWith("مصرف در بارداری") &&*/ (splitedText[i].startsWith("<strong"))) {
                String s = splitedText[i];
                tempData += s;
                if(!mode.equals("p"))
                    mode = "bold";
            }
            else {
//                mode = "p";
            }

        }

        Log.d("\n F:", "finish");
    }

    private void launchHomeScreen() {
        startActivity(new Intent(WelcomePage.this, MainActivity.class));
        finish();
    }

    private void launchWelcomeActivity() {
        startActivity(new Intent(WelcomePage.this, WelcomeActivity.class));
        finish();
    }

    private void checkImage() {
        DisplayMetrics displayMetrics = getApplicationContext().getResources().getDisplayMetrics();
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) vh.logo.getLayoutParams();
        params.width = displayMetrics.widthPixels;
        params.height = (int) (displayMetrics.widthPixels * 1.33);
        vh.logo.setLayoutParams(params);
        vh.logo.setScaleType(ImageView.ScaleType.FIT_XY);
    }

    private static class ViewHolder {
        ImageView logo;
        LinearLayout r1;
    }
}
