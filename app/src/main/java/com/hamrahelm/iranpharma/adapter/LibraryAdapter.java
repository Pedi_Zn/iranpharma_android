package com.hamrahelm.iranpharma.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.hamrahelm.iranpharma.HomeFragment;
import com.hamrahelm.iranpharma.MainActivity;
import com.hamrahelm.iranpharma.ProfileFragment;
import com.hamrahelm.iranpharma.R;
import com.hamrahelm.iranpharma.StorePage;
import com.hamrahelm.iranpharma.Tools;
import com.hamrahelm.iranpharma.model.Book;
import com.hamrahelm.iranpharma.model.Library;
import com.hamrahelm.iranpharma.model.Vitrine;
import com.hamrahelm.iranpharma.network.RetrofitClientInstance;

import java.util.List;

public class LibraryAdapter  extends BaseAdapter {
    public List<Library> libraryList;
    public Context context;
    private LibraryAdapter.ViewHolder vh;

    public LibraryAdapter(Context context, List<Library> libraryList) {
        this.context = context;
        this.libraryList = libraryList;
    }

    public int getCount() {
        return libraryList.size();
    }

    public Library getItem(int position) {
        return libraryList.get(position);
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView != null) {
            vh = (LibraryAdapter.ViewHolder) convertView.getTag();
        } else {
            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = vi.inflate(R.layout.list_item_exbook, parent, false);

            vh = new LibraryAdapter.ViewHolder();
            vh.l1 = convertView.findViewById(R.id.l1);
            vh.title = convertView.findViewById(R.id.title);
            vh.image = convertView.findViewById(R.id.image);
            vh.imageLayout = convertView.findViewById(R.id.imageLayout);
            vh.date = convertView.findViewById(R.id.date);

            Tools.setBoldFont(vh.title);
            Tools.setFont(vh.date);

            makeImage();

            convertView.setTag(vh);
        }

        Library library = getItem(position);
        if (library != null) {
            Glide.with(context)
                    .load(RetrofitClientInstance.BASE_URL + library.getIcon())
                    .apply(new RequestOptions()
                            .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                            .diskCacheStrategy(DiskCacheStrategy.NONE))
                    .transition(DrawableTransitionOptions.withCrossFade(150))
                    .listener(new RequestListener<Drawable>() {
                        @Override
                        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                            return false;
                        }

                        @Override
                        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                            return false;
                        }
                    })
                    .into(vh.image);

            vh.date.setText(Tools.convertNumbersToPersian(library.getJdate_expire_at().split(" ")[0]));

        }

        return convertView;
    }

    private void makeImage() {
        ViewGroup.LayoutParams layoutParams = vh.l1.getLayoutParams();
        layoutParams.width = (int) ProfileFragment.bookWidth;
        vh.l1.setLayoutParams(layoutParams);

        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) vh.imageLayout.getLayoutParams();
        params.width = (int) ProfileFragment.bookWidth;
        params.height = (int) ProfileFragment.bookWidth;
        vh.imageLayout.setLayoutParams(params);
    }

    private class ViewHolder {
        LinearLayout l1;
        TextView title;
        TextView date;
        LinearLayout imageLayout;
        ImageView image;
    }

}
