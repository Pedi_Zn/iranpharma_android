package com.hamrahelm.iranpharma;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.hamrahelm.iranpharma.adapter.CategoryAdapter;
import com.hamrahelm.iranpharma.adapter.DrugAdapter;
import com.hamrahelm.iranpharma.adapter.SearchDrugAdapter;
import com.hamrahelm.iranpharma.adapter.SubCategoryAdapter;
import com.hamrahelm.iranpharma.adapter.VitrineAdapter;
import com.hamrahelm.iranpharma.model.Category;
import com.hamrahelm.iranpharma.model.DrugCard;
import com.hamrahelm.iranpharma.model.DrugPage;
import com.hamrahelm.iranpharma.model.Library;
import com.hamrahelm.iranpharma.model.Vitrine;
import com.hamrahelm.iranpharma.network.GetDataService;
import com.hamrahelm.iranpharma.network.RetrofitClientInstance;
import com.google.android.material.button.MaterialButton;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Pedi on 8/13/2019 AD.
 */

public class DrugFragment2 extends Fragment implements OnBackPressed {

    private int PAGINATION = 1300;
    private int PAGE = 1;
    public static int STATE = 0;
    public static final int CATEGORY = 1;
    public static final int SUB_CATEGORY = 2;
    public static final int DRUGS = 3;

    public static int drugModel;
    public static int drugTitle;

    public int categoryId;
    public String categoryTitle;
    public String categoryModel;

    public int subCategoryId;
    public String subCategoryTitle;

    private View root;
    private ViewHolder vh;
    public static DrugCard drug;
    private List<DrugCard> drugList;
    public static int drugId;
    public static String drugName;
    private PrefManager prefManager;
    boolean doubleBackToExitPressedOnce = false;

    public static String searchText;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefManager = new PrefManager(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (root == null) {
            root = inflater.inflate(R.layout.fragment_drug, container, false);
            initViewHolder(root);
        }
        return root;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void refreshData() {
        initViewHolder(root);
    }

    @Override
    public void onResume() {
        super.onResume();
        initViewHolder(root);
        checkNetwork();
    }

    @Override
    public boolean onBackPressed() {
        if (STATE == CATEGORY) {
            if (doubleBackToExitPressedOnce) {
                return true;
            }

            this.doubleBackToExitPressedOnce = true;
            Tools.displayToast(getContext(), "برای خروج، مجدداً کلیک کنید");

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    doubleBackToExitPressedOnce = false;
                }
            }, 2000);
        } else {
            backPressed();
            return false;
        }
        return false;
    }

    private void initViewHolder(View view) {
        vh = new ViewHolder();
        vh.title = view.findViewById(R.id.title);
        vh.back = view.findViewById(R.id.back);
        vh.progressbar = view.findViewById(R.id.progressbar);

        vh.internet_connection = view.findViewById(R.id.internet_connection);
        vh.internetMessage = view.findViewById(R.id.internetMessage);
        vh.tryAgain = view.findViewById(R.id.tryAgain);

        vh.searchLayout = view.findViewById(R.id.searchLayout);
        vh.search = view.findViewById(R.id.search);
        vh.searchButton = view.findViewById(R.id.searchButton);

        vh.listLayout = view.findViewById(R.id.listLayout);
        vh.listView = view.findViewById(R.id.listView);

        vh.not_found = view.findViewById(R.id.not_found);
        vh.not_found_text = view.findViewById(R.id.not_found_text);
        vh.empty_library = view.findViewById(R.id.empty_library);
        vh.empty_library_text = view.findViewById(R.id.empty_library_text);


        vh.back.setVisibility(View.GONE);
        vh.title.setText("کتابخانه من");

        Tools.setFont(vh.internetMessage);
        Tools.setFont(vh.tryAgain);

        Tools.setBoldFont(vh.title);
        Tools.setFont(vh.search);

        Tools.setBoldFont(vh.not_found_text);
        Tools.setBoldFont(vh.empty_library_text);

        vh.internet_connection.setVisibility(View.GONE);
        vh.listLayout.setVisibility(View.GONE);
        vh.listView.setVisibility(View.GONE);

        STATE = CATEGORY;

        if (Tools.isConnected(getContext())) {
            if (searchText != null) {
                searchProcess();
            }
            else {
                showProgressBar();
                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<List<Library>> call = service.library(prefManager.getAccessToken());
                call.enqueue(new Callback<List<Library>>() {
                    @Override
                    public void onResponse(Call<List<Library>> call, final Response<List<Library>> response) {
                        hideProgressBar();
                        if (response.code() == 403) {
                            vh.listLayout.setVisibility(View.VISIBLE);
                            vh.empty_library.setVisibility(View.VISIBLE);
                            vh.not_found.setVisibility(View.GONE);
                            vh.listView.setVisibility(View.GONE);
                        }
                        if (response.code() == 200) {
                            if (response.body() != null) {
                                if (response.body().size() == 0) {
                                    vh.listLayout.setVisibility(View.VISIBLE);
                                    vh.empty_library.setVisibility(View.VISIBLE);
                                } else {
                                    vh.empty_library.setVisibility(View.GONE);
                                    vh.not_found.setVisibility(View.GONE);
                                    vh.listLayout.setVisibility(View.VISIBLE);
                                    vh.listView.setVisibility(View.VISIBLE);
                                    vh.listView.setFastScrollEnabled(false);
                                    vh.listView.setAdapter(new CategoryAdapter(getContext(), response.body()));
                                    vh.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                        @Override
                                        public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {
                                            if (response.body().get(i).getId() != null &&
                                                    response.body().get(i).getTitle() != null &&
                                                    response.body().get(i).getModel() != null) {

                                                categoryId = response.body().get(i).getId();
                                                categoryTitle = response.body().get(i).getTitle();
                                                categoryModel = response.body().get(i).getModel();

                                                vh.listView.setVisibility(View.GONE);

                                                if (categoryModel.equals("book")) {
                                                    STATE = SUB_CATEGORY;
                                                    vh.title.setText(categoryTitle);
                                                    vh.back.setVisibility(View.VISIBLE);
                                                    vh.back.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {
                                                            backPressed();
                                                        }
                                                    });
                                                    if (Tools.isConnected(getContext())) {
                                                        showProgressBar();
                                                        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                                                        Call<Category> call = service.category(categoryId);
                                                        call.enqueue(new Callback<Category>() {
                                                            @Override
                                                            public void onResponse(Call<Category> call, final Response<Category> response) {
                                                                hideProgressBar();
                                                                if (response.body() != null) {
                                                                    vh.listView.setVisibility(View.VISIBLE);
                                                                    vh.listView.setFastScrollEnabled(false);
                                                                    vh.listView.setAdapter(new SubCategoryAdapter(getContext(), response.body().getSubCategories()));
                                                                    vh.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                                                        @Override
                                                                        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                                                                            subCategoryId = response.body().getSubCategories().get(i).getId();
                                                                            subCategoryTitle = response.body().getSubCategories().get(i).getTitle();

                                                                            STATE = DRUGS;
                                                                            vh.listView.setVisibility(View.GONE);
                                                                            vh.title.setText(subCategoryTitle);
                                                                            vh.back.setVisibility(View.VISIBLE);
                                                                            vh.back.setOnClickListener(new View.OnClickListener() {
                                                                                @Override
                                                                                public void onClick(View view) {
                                                                                    backPressed();
                                                                                }
                                                                            });
                                                                            showProgressBar();
                                                                            GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                                                                            Call<DrugPage> call = service.drugPage(subCategoryId, PAGINATION);
                                                                            call.enqueue(new Callback<DrugPage>() {
                                                                                @Override
                                                                                public void onResponse(Call<DrugPage> call, final Response<DrugPage> response) {
                                                                                    hideProgressBar();
                                                                                    if (response.body() != null) {
                                                                                        vh.listView.setVisibility(View.VISIBLE);
                                                                                        vh.listView.setFastScrollEnabled(true);
                                                                                        vh.listView.setAdapter(new DrugAdapter(getContext(), response.body().getData()));
                                                                                        vh.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                                                                            @Override
                                                                                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                                                                                        vh.listView.setVisibility(View.GONE);
                                                                                                drugId = response.body().getData().get(i).getId();
                                                                                                drugName = response.body().getData().get(i).getTitle();
                                                                                                Intent intent = new Intent(getActivity(), DrugsPage.class);
                                                                                                startActivity(intent);
                                                                                            }
                                                                                        });
                                                                                    }
                                                                                }

                                                                                @Override
                                                                                public void onFailure(Call<DrugPage> call, Throwable t) {
                                                                                    hideProgressBar();
                                                                                    Tools.displayErrorToast(getContext(), getResources().getString(R.string.error));
                                                                                }
                                                                            });
                                                                        }
                                                                    });
                                                                }
                                                            }

                                                            @Override
                                                            public void onFailure(Call<Category> call, Throwable t) {
                                                                hideProgressBar();
                                                                Tools.displayErrorToast(getContext(), getResources().getString(R.string.error));
                                                            }
                                                        });
                                                    } else {
                                                        vh.internet_connection.setVisibility(View.VISIBLE);
                                                        vh.tryAgain.setOnClickListener(new View.OnClickListener() {
                                                            @Override
                                                            public void onClick(View view) {
                                                                onResume();
                                                            }
                                                        });
                                                    }
                                                }
                                                if (categoryModel.equals("minibook")) {
                                                    STATE = SUB_CATEGORY;
                                                    vh.title.setText(categoryTitle);
                                                    vh.back.setVisibility(View.VISIBLE);
                                                    vh.back.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {
                                                            backPressed();
                                                        }
                                                    });
                                                    showProgressBar();
                                                    GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                                                    Call<DrugPage> call = service.drugPage(categoryId, PAGINATION);
                                                    call.enqueue(new Callback<DrugPage>() {
                                                        @Override
                                                        public void onResponse(Call<DrugPage> call, final Response<DrugPage> response) {
                                                            hideProgressBar();
                                                            if (response.body() != null) {
                                                                vh.listView.setVisibility(View.VISIBLE);
                                                                vh.listView.setFastScrollEnabled(true);
                                                                vh.listView.setAdapter(new DrugAdapter(getContext(), response.body().getData()));
                                                                vh.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                                                    @Override
                                                                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                                                                    vh.listView.setVisibility(View.GONE);
                                                                        drugId = response.body().getData().get(i).getId();
                                                                        drugName = response.body().getData().get(i).getTitle();
                                                                        Intent intent = new Intent(getActivity(), DrugsPage.class);
                                                                        startActivity(intent);
                                                                    }
                                                                });
                                                            }
                                                        }

                                                        @Override
                                                        public void onFailure(Call<DrugPage> call, Throwable t) {
                                                            hideProgressBar();
                                                            Tools.displayErrorToast(getContext(), getResources().getString(R.string.error));
                                                        }
                                                    });
                                                }
                                                if (categoryModel.equals("sayer")) {
                                                    STATE = SUB_CATEGORY;
                                                    vh.title.setText(categoryTitle);
                                                    vh.back.setVisibility(View.VISIBLE);
                                                    vh.back.setOnClickListener(new View.OnClickListener() {
                                                        @Override
                                                        public void onClick(View view) {
                                                            backPressed();
                                                        }
                                                    });
                                                    showProgressBar();
                                                    GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                                                    Call<DrugPage> call = service.drugPage(categoryId, PAGINATION);
                                                    call.enqueue(new Callback<DrugPage>() {
                                                        @Override
                                                        public void onResponse(Call<DrugPage> call, final Response<DrugPage> response) {
                                                            hideProgressBar();
                                                            if (response.body() != null) {
                                                                vh.listView.setVisibility(View.VISIBLE);
                                                                vh.listView.setFastScrollEnabled(true);
                                                                vh.listView.setAdapter(new DrugAdapter(getContext(), response.body().getData()));
                                                                vh.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                                                    @Override
                                                                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                                                                    vh.listView.setVisibility(View.GONE);
                                                                        drugId = response.body().getData().get(i).getId();
                                                                        drugName = response.body().getData().get(i).getTitle();
                                                                        Intent intent = new Intent(getActivity(), DrugsPage.class);
                                                                        startActivity(intent);
                                                                    }
                                                                });
                                                            }
                                                        }

                                                        @Override
                                                        public void onFailure(Call<DrugPage> call, Throwable t) {
                                                            hideProgressBar();
                                                            Tools.displayErrorToast(getContext(), getResources().getString(R.string.error));
                                                        }
                                                    });
                                                }

                                            }
                                        }
                                    });
                                }
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<List<Library>> call, Throwable t) {
                        vh.title.setText(R.string.drugs);
                        hideProgressBar();
                        Tools.displayErrorToast(getContext(), getResources().getString(R.string.error));
                    }
                });
            }

            vh.search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {
                    boolean handled = false;
                    if (i == EditorInfo.IME_ACTION_SEARCH) {
                        handled = true;
                        if (Tools.isConnected(getContext())) {
                            searchText = vh.search.getText().toString();
                            vh.internet_connection.setVisibility(View.GONE);
                            if (vh.search.getText().length() != 0) {
                                vh.empty_library.setVisibility(View.GONE);
                                vh.not_found.setVisibility(View.GONE);
                                showProgressBar();
                                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                                Call<DrugPage> call = service.search(prefManager.getAccessToken(), searchText, PAGINATION, PAGE);
                                call.enqueue(new Callback<DrugPage>() {
                                    @Override
                                    public void onResponse(Call<DrugPage> call, final Response<DrugPage> response) {
                                        hideProgressBar();
                                        if (response.body() != null) {
                                            vh.listView.setVisibility(View.VISIBLE);
                                            drugList = response.body().getData();
                                            if (drugList.size() == 0) {
                                                vh.listView.setVisibility(View.GONE);
                                                vh.not_found.setVisibility(View.VISIBLE);
                                            } else {
                                                vh.listView.setAdapter(new SearchDrugAdapter(getContext(), drugList));
                                                vh.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                                    @Override
                                                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                                        if (Tools.isConnected(getContext())) {
                                                            if (drugList.get(i).getHasMedicine() == true) {
//                                                                vh.listView.setVisibility(View.GONE);
                                                                drugId = response.body().getData().get(i).getId();
                                                                drugName = response.body().getData().get(i).getTitle();
                                                                Intent intent = new Intent(getActivity(), DrugsPage.class);
                                                                startActivity(intent);
                                                            }
                                                            else {
//                                                            ((MainActivity) getContext()).setSelectedFragment(0);
                                                                Tools.displayErrorToast(getContext(), "دارو در کتابخانه شما وجود ندارد");

                                                                if (response.body().getData().get(i).getModel().equals("book")) {
                                                                    drugModel = 0;
                                                                }
                                                                if (response.body().getData().get(i).getModel().equals("minibook")) {
                                                                    drugModel = 1;
                                                                }
                                                                if (response.body().getData().get(i).getModel().equals("sayer")) {
                                                                    drugModel = 2;
                                                                }

                                                                if (response.body().getData().get(i).getModel_Id().equals("1") ||
                                                                        response.body().getData().get(i).getModel_Id().equals("220")||
                                                                        response.body().getData().get(i).getModel_Id().equals("241")) {
                                                                    drugTitle = 0;
                                                                }
                                                                if (response.body().getData().get(i).getModel_Id().equals("221")) {
                                                                    drugTitle = 1;
                                                                }
                                                                if (response.body().getData().get(i).getModel_Id().equals("222")) {
                                                                    drugTitle = 2;
                                                                }
                                                                if (response.body().getData().get(i).getModel_Id().equals("223")) {
                                                                    drugTitle = 3;
                                                                }
                                                                if (response.body().getData().get(i).getModel_Id().equals("224")) {
                                                                    drugTitle = 4;
                                                                }
                                                                if (response.body().getData().get(i).getModel_Id().equals("225")) {
                                                                    drugTitle = 5;
                                                                }
                                                                if (response.body().getData().get(i).getModel_Id().equals("226")) {
                                                                    drugTitle = 6;
                                                                }
                                                                if (response.body().getData().get(i).getModel_Id().equals("227")) {
                                                                    drugTitle = 7;
                                                                }
                                                                if (response.body().getData().get(i).getModel_Id().equals("228")) {
                                                                    drugTitle = 8;
                                                                }
                                                                if (response.body().getData().get(i).getModel_Id().equals("231")) {
                                                                    drugTitle = 9;
                                                                }

                                                                showProgressBar();
                                                                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                                                                Call<List<Vitrine>> call = service.vitrine(prefManager.getAccessToken());
                                                                call.enqueue(new Callback<List<Vitrine>>() {
                                                                    @Override
                                                                    public void onResponse(Call<List<Vitrine>> call, Response<List<Vitrine>> response) {
                                                                        if (response.body() != null) {
                                                                            hideProgressBar();
                                                                            VitrineAdapter.STATE = VitrineAdapter.BUY;
                                                                            VitrineAdapter.book = response.body().get(drugModel).getData().get(drugTitle);
                                                                            VitrineAdapter.bookId = VitrineAdapter.book.getId();
                                                                            VitrineAdapter.bookPrice = VitrineAdapter.book.getPrice();
                                                                            VitrineAdapter.bookDesc = VitrineAdapter.book.getDescription();
                                                                            VitrineAdapter.quantity = VitrineAdapter.book.getQuantity();
                                                                            VitrineAdapter.icon = VitrineAdapter.book.getIcon();
                                                                            VitrineAdapter.allBooksPrice = response.body().get(drugModel).getAllPrice();
                                                                            if (response.body().get(drugModel).getTitle().equals("درسنامه")) {
                                                                                VitrineAdapter.bookTitle = "درسنامه";
                                                                                VitrineAdapter.bookModel = "category";
                                                                            }
                                                                            if (response.body().get(drugModel).getTitle().equals("دستنامه")) {
                                                                                VitrineAdapter.bookTitle = "دستنامه";
                                                                                VitrineAdapter.bookModel = "sub_category";
                                                                            }
                                                                            if (response.body().get(drugModel).getTitle().equals("سایر")) {
                                                                                VitrineAdapter.bookTitle = "سایر";
                                                                                VitrineAdapter.bookModel = "sub_category";
                                                                            }
                                                                            Intent intent = new Intent(getContext(), StorePage.class);
                                                                            getContext().startActivity(intent);
                                                                        }
                                                                    }

                                                                    @Override
                                                                    public void onFailure(Call<List<Vitrine>> call, Throwable t) {
                                                                        Tools.displayErrorToast(getContext(), getResources().getString(R.string.error));
                                                                    }
                                                                });
                                                            }
                                                        } else {
                                                            vh.internet_connection.setVisibility(View.VISIBLE);
                                                            vh.tryAgain.setOnClickListener(new View.OnClickListener() {
                                                                @Override
                                                                public void onClick(View view) {
                                                                    onResume();
                                                                }
                                                            });
                                                        }
                                                    }
                                                });
                                            }
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<DrugPage> call, Throwable t) {
                                        vh.listView.setVisibility(View.GONE);
                                        hideProgressBar();
                                        Tools.displayErrorToast(getContext(), getResources().getString(R.string.error));
                                    }
                                });
                            } else {
                                vh.not_found.setVisibility(View.VISIBLE);
                            }
                        } else {
                            vh.internet_connection.setVisibility(View.VISIBLE);
                            vh.tryAgain.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    onResume();
                                }
                            });
                        }
                    }
                    return handled;
                }
            });

            vh.searchButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Tools.isConnected(getContext())) {
                        searchProcess();

                    } else {
                        vh.internet_connection.setVisibility(View.VISIBLE);
                        vh.tryAgain.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                onResume();
                            }
                        });
                    }
                }
            });
        } else {
            vh.internet_connection.setVisibility(View.VISIBLE);
            vh.tryAgain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onResume();
                }
            });
        }
    }

    public void searchProcess() {
        vh.listView.setVisibility(View.VISIBLE);
        searchText = vh.search.getText().toString();
        vh.internet_connection.setVisibility(View.GONE);
        if (searchText.length() != 0) {
            vh.empty_library.setVisibility(View.GONE);
            vh.not_found.setVisibility(View.GONE);
            showProgressBar();
            GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
            Call<DrugPage> call = service.search(prefManager.getAccessToken(), searchText, PAGINATION, PAGE);
            call.enqueue(new Callback<DrugPage>() {
                @Override
                public void onResponse(Call<DrugPage> call, final Response<DrugPage> response) {
                    hideProgressBar();
                    if (response.body() != null) {
                        vh.listView.setVisibility(View.VISIBLE);
                        drugList = response.body().getData();
                        if (drugList.size() == 0) {
                            vh.listView.setVisibility(View.GONE);
                            vh.not_found.setVisibility(View.VISIBLE);
                        }
                        else {
                            vh.listView.setAdapter(new SearchDrugAdapter(getContext(), drugList));
                            vh.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                @Override
                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                    if (Tools.isConnected(getContext())) {
                                        if (drugList.get(i).getHasMedicine() == true) {
//                                                            vh.listView.setVisibility(View.GONE);
                                            drugId = response.body().getData().get(i).getId();
                                            drugName = response.body().getData().get(i).getTitle();
                                            Intent intent = new Intent(getActivity(), DrugsPage.class);
                                            startActivity(intent);
                                        }
                                        else {
//                                                            ((MainActivity) getContext()).setSelectedFragment(0);
                                            Tools.displayErrorToast(getContext(), "دارو در کتابخانه شما وجود ندارد");

                                            if (response.body().getData().get(i).getModel().equals("book")) {
                                                drugModel = 0;
                                            }
                                            if (response.body().getData().get(i).getModel().equals("minibook")) {
                                                drugModel = 1;
                                            }
                                            if (response.body().getData().get(i).getModel().equals("sayer")) {
                                                drugModel = 2;
                                            }

                                            if (response.body().getData().get(i).getModel_Id().equals("1") ||
                                                    response.body().getData().get(i).getModel_Id().equals("220")||
                                                    response.body().getData().get(i).getModel_Id().equals("241")) {
                                                drugTitle = 0;
                                            }
                                            if (response.body().getData().get(i).getModel_Id().equals("221")) {
                                                drugTitle = 1;
                                            }
                                            if (response.body().getData().get(i).getModel_Id().equals("222")) {
                                                drugTitle = 2;
                                            }
                                            if (response.body().getData().get(i).getModel_Id().equals("223")) {
                                                drugTitle = 3;
                                            }
                                            if (response.body().getData().get(i).getModel_Id().equals("224")) {
                                                drugTitle = 4;
                                            }
                                            if (response.body().getData().get(i).getModel_Id().equals("225")) {
                                                drugTitle = 5;
                                            }
                                            if (response.body().getData().get(i).getModel_Id().equals("226")) {
                                                drugTitle = 6;
                                            }
                                            if (response.body().getData().get(i).getModel_Id().equals("227")) {
                                                drugTitle = 7;
                                            }
                                            if (response.body().getData().get(i).getModel_Id().equals("228")) {
                                                drugTitle = 8;
                                            }
                                            if (response.body().getData().get(i).getModel_Id().equals("231")) {
                                                drugTitle = 9;
                                            }

                                            showProgressBar();
                                            GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                                            Call<List<Vitrine>> call = service.vitrine(prefManager.getAccessToken());
                                            call.enqueue(new Callback<List<Vitrine>>() {
                                                @Override
                                                public void onResponse(Call<List<Vitrine>> call, Response<List<Vitrine>> response) {
                                                    if (response.body() != null) {
                                                        hideProgressBar();
                                                        VitrineAdapter.STATE = VitrineAdapter.BUY;
                                                        VitrineAdapter.book = response.body().get(drugModel).getData().get(drugTitle);
                                                        VitrineAdapter.bookId = VitrineAdapter.book.getId();
                                                        VitrineAdapter.bookPrice = VitrineAdapter.book.getPrice();
                                                        VitrineAdapter.bookDesc = VitrineAdapter.book.getDescription();
                                                        VitrineAdapter.quantity = VitrineAdapter.book.getQuantity();
                                                        VitrineAdapter.icon = VitrineAdapter.book.getIcon();
                                                        VitrineAdapter.allBooksPrice = response.body().get(drugModel).getAllPrice();
                                                        if (response.body().get(drugModel).getTitle().equals("درسنامه")) {
                                                            VitrineAdapter.bookTitle = "درسنامه";
                                                            VitrineAdapter.bookModel = "category";
                                                        }
                                                        if (response.body().get(drugModel).getTitle().equals("دستنامه")) {
                                                            VitrineAdapter.bookTitle = "دستنامه";
                                                            VitrineAdapter.bookModel = "sub_category";
                                                        }
                                                        if (response.body().get(drugModel).getTitle().equals("سایر")) {
                                                            VitrineAdapter.bookTitle = "سایر";
                                                            VitrineAdapter.bookModel = "sub_category";
                                                        }
                                                        Intent intent = new Intent(getContext(), StorePage.class);
                                                        getContext().startActivity(intent);
                                                    }
                                                }

                                                @Override
                                                public void onFailure(Call<List<Vitrine>> call, Throwable t) {
                                                    Tools.displayErrorToast(getContext(), getResources().getString(R.string.error));
                                                }
                                            });
                                        }
                                    } else {
                                        vh.internet_connection.setVisibility(View.VISIBLE);
                                        vh.tryAgain.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                onResume();
                                            }
                                        });
                                    }
                                }
                            });

                        }
                    }
                }

                @Override
                public void onFailure(Call<DrugPage> call, Throwable t) {
                    vh.listView.setVisibility(View.GONE);
                    hideProgressBar();
                    Tools.displayErrorToast(getContext(), getResources().getString(R.string.error));
                }
            });
        }
    }

    private void backPressed() {
        if (STATE == SUB_CATEGORY) {
            initViewHolder(root);
        }
        if (STATE == DRUGS) {
            STATE = SUB_CATEGORY;
            vh.title.setText(categoryTitle);
            vh.back.setVisibility(View.VISIBLE);
            vh.back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    backPressed();
                }
            });
            vh.listView.setVisibility(View.GONE);
            showProgressBar();
            GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
            Call<Category> call = service.category(categoryId);
            call.enqueue(new Callback<Category>() {
                @Override
                public void onResponse(Call<Category> call, final Response<Category> response) {
                    hideProgressBar();
                    if (response.body() != null) {
                        vh.listView.setVisibility(View.VISIBLE);
                        vh.listView.setAdapter(new SubCategoryAdapter(getContext(), response.body().getSubCategories()));
                        vh.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                                subCategoryId = response.body().getSubCategories().get(i).getId();
                                subCategoryTitle = response.body().getSubCategories().get(i).getTitle();

                                STATE = DRUGS;
                                vh.listView.setVisibility(View.GONE);
                                vh.title.setText(subCategoryTitle);
                                vh.back.setVisibility(View.VISIBLE);
                                vh.back.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        backPressed();
                                    }
                                });
                                showProgressBar();
                                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                                Call<DrugPage> call = service.drugPage(subCategoryId, PAGINATION);
                                call.enqueue(new Callback<DrugPage>() {
                                    @Override
                                    public void onResponse(Call<DrugPage> call, final Response<DrugPage> response) {
                                        hideProgressBar();
                                        if (response.body() != null) {
                                            vh.listView.setVisibility(View.VISIBLE);
                                            vh.listView.setFastScrollEnabled(true);
                                            vh.listView.setAdapter(new DrugAdapter(getContext(), response.body().getData()));
                                            vh.listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                                                @Override
                                                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                                                    vh.listView.setVisibility(View.GONE);
                                                    drugId = response.body().getData().get(i).getId();
                                                    drugName = response.body().getData().get(i).getTitle();
                                                    Intent intent = new Intent(getActivity(), DrugsPage.class);
                                                    startActivity(intent);
                                                }
                                            });
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<DrugPage> call, Throwable t) {
                                        hideProgressBar();
                                        Tools.displayErrorToast(getContext(), getResources().getString(R.string.error));
                                    }
                                });
                            }
                        });
                    }
                }

                @Override
                public void onFailure(Call<Category> call, Throwable t) {
                    hideProgressBar();
                    Tools.displayErrorToast(getContext(), getResources().getString(R.string.error));
                }
            });
        }
    }

    private void showProgressBar() {
        if (Tools.isConnected(getContext())) {
            vh.progressbar.setVisibility(View.VISIBLE);
            vh.progressbar.setBackgroundColor(getResources().getColor(R.color.white));
        } else {
            vh.progressbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        }
    }

    private void hideProgressBar() {
        vh.progressbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
    }

    private void checkNetwork() {
        if (Tools.isConnected(getContext())) {
            vh.internet_connection.setVisibility(View.GONE);
        } else {
            vh.internet_connection.setVisibility(View.VISIBLE);
            vh.tryAgain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onResume();
                }
            });
        }
    }

    public static class ViewHolder {
        ProgressBar progressbar;

        RelativeLayout internet_connection;
        TextView internetMessage;
        MaterialButton tryAgain;

        ImageView back;
        TextView title;
        EditText search;
        ImageView searchButton;

        LinearLayout searchLayout;
        LinearLayout listLayout;

        ListView listView;

        RelativeLayout not_found;
        RelativeLayout empty_library;
        TextView not_found_text;
        TextView empty_library_text;
    }
}
