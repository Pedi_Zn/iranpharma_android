package com.hamrahelm.iranpharma.network;

import com.hamrahelm.iranpharma.model.Category;
import com.hamrahelm.iranpharma.model.CategoryPage;
import com.hamrahelm.iranpharma.model.DrugPage;
import com.hamrahelm.iranpharma.model.Library;
import com.hamrahelm.iranpharma.model.Login;
import com.hamrahelm.iranpharma.model.Logout;
import com.hamrahelm.iranpharma.model.Pay;
import com.hamrahelm.iranpharma.model.Profile;
import com.hamrahelm.iranpharma.model.Register;
import com.hamrahelm.iranpharma.model.Verification;
import com.hamrahelm.iranpharma.model.Version;
import com.hamrahelm.iranpharma.model.Vitrine;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface GetDataService {

    @POST("api/auth/login")
    @FormUrlEncoded
    Call<Login> login(@Field("phone") String phone,
                      @Field("password") String password);

    @POST("api/auth/user_register")
    @FormUrlEncoded
    Call<Register> register(@Field("tracking_code") String tracking_code,
                            @Field("code") String code,
                            @Field("title") String title,
                            @Field("phone") String phone,
                            @Field("password") String password);

    @POST("api/auth/forgot-password")
    @FormUrlEncoded
    Call<Register> forgotPassword(@Field("tracking_code") String tracking_code,
                            @Field("code") String code,
                            @Field("password") String password);

    @POST("api/profile")
    @FormUrlEncoded
    Call<Profile> editProfile(@Header("Authorization") String authorization,
                               @Field("firstname") String firstname,
                               @Field("lastname") String lastname,
                               @Field("email") String email);

    @POST("api/auth/change-password")
    @FormUrlEncoded
    Call<Register> changePassword(@Header("Authorization") String authorization,
                               @Field("old_password") String oldPassword,
                               @Field("password") String password);

    @POST("api/add-library")
    @FormUrlEncoded
    Call<Register> buy(@Header("Authorization") String authorization,
                       @Field("user_id") int userId,
                       @Field("sub_category_id") int subCategoryId,
                       @Field("day") int day);

    @POST("api/auth/verify")
    @FormUrlEncoded
    Call<Verification> verification(@Field("phone") String phone);

    @GET("api/auth/logout")
    Call<Logout> logout(@Header("Authorization") String authorization);

    @GET("api/version")
    Call<Version> version();

    @GET("api/profile")
    Call<Profile> profile(@Header("Authorization") String authorization);

    @GET("api/category/{id}")
    Call<Category> category(@Path("id") Integer id);

    @GET("api/show-medicines")
    Call<DrugPage> drugPage(@Query("sub_category") Integer id, @Query("pagination") Integer pagination);

    @GET("api/medicine-user/{id}")
    Call<Logout> drug(@Path("id") Integer id, @Header("Authorization") String authorization);

    @GET("api/search")
    Call<DrugPage> search(@Header("Authorization") String authorization, @Query("search") String search, @Query("pagination") Integer pagination, @Query("page") Integer page);

    @GET("api/pay")
    Call<Pay> pay(@Header("Authorization") String authorization, @Query("id") String id, @Query("model") String model);

    @GET("api/vitrine")
    Call<List<Vitrine>> vitrine(@Header("Authorization") String authorization);

    @GET("api/library")
    Call<List<Library>> library(@Header("Authorization") String authorization);
}
