package com.hamrahelm.iranpharma;

import android.app.Application;
import android.content.Context;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Handler;
import android.security.keystore.KeyProperties;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Pedi on 18-08-2019.
 */

public class Tools extends Application {

    public static final String AES_MODE = KeyProperties.KEY_ALGORITHM_AES + "/" + KeyProperties.BLOCK_MODE_CBC + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7;
    public static final String PASS_CHARSET = "ISO-8859-1";

    public static volatile Handler applicationHandler;
    public static volatile Context applicationContext;
    public static volatile Typeface custom_font;
    public static volatile Typeface custom_bold_font;
    public static volatile Typeface custom_medium_font;

    @Override
    public void onCreate() {
        super.onCreate();

        startTimer();
        applicationContext = getApplicationContext();
        logTime("1");
        applicationHandler = new Handler(getApplicationContext().getMainLooper());
        logTime("2");
        setCustomFont(applicationContext);
    }

    public static void setCustomFont(Context context) {
        custom_font = Typeface.createFromAsset(context.getAssets(), "fonts/iransansmobile_light.ttf");
        custom_bold_font = Typeface.createFromAsset(context.getAssets(), "fonts/iransansmobile_bold.ttf");
        custom_medium_font = Typeface.createFromAsset(context.getAssets(), "fonts/iransansmobile_light.ttf");
    }

    public static void setFont(Object object) {

        if (object instanceof Button)
            ((Button) object).setTypeface(custom_font);
        if (object instanceof EditText)
            ((EditText) object).setTypeface(custom_font);
        if (object instanceof TextView)
            ((TextView) object).setTypeface(custom_font);
        if (object instanceof CheckBox)
            ((CheckBox) object).setTypeface(custom_font);
    }

    public static void setBoldFont(Object object) {

        if (object instanceof Button)
            ((Button) object).setTypeface(custom_bold_font);
        if (object instanceof EditText)
            ((EditText) object).setTypeface(custom_bold_font);
        if (object instanceof TextView)
            ((TextView) object).setTypeface(custom_bold_font);
    }

    public static void displayToast(Context context, final String message) {
        displayToast(context, message, Toast.LENGTH_SHORT);
    }

    public static void displayToast(final Context context, final String message, final int length) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.toast_normal, null);

        TextView text = (TextView) layout.findViewById(R.id.message);
        text.setText(message);
        setFont(text);

        Toast toast = new Toast(context);
        toast.setGravity(Gravity.BOTTOM, 0, 100);
        toast.setDuration(length);
        toast.setView(layout);
        toast.show();
    }

    public static void displayErrorToast(Context context, final String message) {
        displayErrorToast(context, message, Toast.LENGTH_SHORT);
    }

    public static void displayErrorToast(Context context, final String message, final int length) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.toast_error, null);

        TextView text = (TextView) layout.findViewById(R.id.message);
        text.setText(message);
        setFont(text);

        Toast toast = new Toast(context);
        toast.setGravity(Gravity.BOTTOM, 0, 100);
        toast.setDuration(length);
        toast.setView(layout);
        toast.show();
    }

    public static boolean isConnected(Context context) {

        ConnectivityManager conMgr = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        /** to get info of WIFI N/W : */
        final android.net.NetworkInfo wifi = conMgr
                .getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        /** to get info of mobile N/W : */
        final android.net.NetworkInfo mobile = conMgr
                .getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if ((wifi.isAvailable() && wifi.isConnected())
                || (mobile.isAvailable() && mobile.isConnected())) {
            Log.i("Is Net work?", "isNetWork:in 'isNetWork_if' is N/W Connected:"
                    + NetworkInfo.State.CONNECTED);
            return true;
        } else if (conMgr.getActiveNetworkInfo() != null
                && conMgr.getActiveNetworkInfo().isAvailable()
                && conMgr.getActiveNetworkInfo().isConnected()) {
            return true;
        }
        return false;
    }

    long startTime;

    public void startTimer() {
        startTime = System.currentTimeMillis();
    }

    public void logTime(String log) {
        Log.v("MAIN_APPLICATION", log + " after " + (System.currentTimeMillis() - startTime));
        startTime = System.currentTimeMillis();
    }

    public static int dpToPx(Context context, int dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static int getScreenHeight(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int pxWidth = displayMetrics.widthPixels;
        int pxHeight = displayMetrics.heightPixels;
        return pxHeight;
    }

    public static String convertNumbersToPersian(int number) {
        return convertNumbersToPersian(String.valueOf(number));
    }

    public static String convertNumbersToPersian(String text) {
        text = text.replace('0', '۰');
        text = text.replace('1', '۱');
        text = text.replace('2', '۲');
        text = text.replace('3', '۳');
        text = text.replace('4', '۴');
        text = text.replace('5', '۵');
        text = text.replace('6', '۶');
        text = text.replace('7', '۷');
        text = text.replace('8', '۸');
        text = text.replace('9', '۹');

        return text;
    }

    public static String convertNumbersToEnglish(String text) {
        text = text.replace('۰', '0');
        text = text.replace('۱', '1');
        text = text.replace('۲', '2');
        text = text.replace('۳', '3');
        text = text.replace('۴', '4');
        text = text.replace('۵', '5');
        text = text.replace('۶', '6');
        text = text.replace('۷', '7');
        text = text.replace('۸', '8');
        text = text.replace('۹', '9');

        text = text.replace('٠', '0');
        text = text.replace('١', '1');
        text = text.replace('٢', '2');
        text = text.replace('٣', '3');
        text = text.replace('٤', '4');
        text = text.replace('٥', '5');
        text = text.replace('٦', '6');
        text = text.replace('٧', '7');
        text = text.replace('٨', '8');
        text = text.replace('٩', '9');

        return text;
    }
}
