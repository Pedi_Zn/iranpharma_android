package com.hamrahelm.iranpharma;

public interface OnBackPressed {
    boolean onBackPressed();
}