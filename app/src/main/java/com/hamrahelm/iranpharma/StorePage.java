package com.hamrahelm.iranpharma;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Layout;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.hamrahelm.iranpharma.adapter.VitrineAdapter;
import com.hamrahelm.iranpharma.model.Pay;
import com.hamrahelm.iranpharma.network.GetDataService;
import com.hamrahelm.iranpharma.network.RetrofitClientInstance;
import com.google.android.material.button.MaterialButton;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StorePage extends AppCompatActivity {

    public static StorePage.ViewHolder vh;
    public String fullImageUrl;
    public String bookModel;
    private PrefManager prefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_page);
        prefManager = new PrefManager(getApplicationContext());
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        initViewHolder();
        checkNetwork();
    }

    private void initViewHolder() {
        vh = new ViewHolder();
        vh.title = findViewById(R.id.title);

        vh.progressbar = findViewById(R.id.progressbar);

        vh.back = findViewById(R.id.back);
        vh.internet_connection = findViewById(R.id.internet_connection);
        vh.internetMessage = findViewById(R.id.internetMessage);
        vh.tryAgain = findViewById(R.id.tryAgain);

        vh.scrollView = findViewById(R.id.scrollView);

        vh.info = findViewById(R.id.info);
        vh.imageView = findViewById(R.id.imageView);
        vh.bookTitle = findViewById(R.id.bookTitle);
        vh.description = findViewById(R.id.description);
        vh.showMore = findViewById(R.id.showMore);
        vh.fullDesc = findViewById(R.id.fullDesc);
        vh.buy = findViewById(R.id.buy);

        vh.allBooks = findViewById(R.id.allBooks);
        vh.allBooksTitle = findViewById(R.id.allBooksTitle);
        vh.buy2 = findViewById(R.id.buy2);
        vh.fullImage = findViewById(R.id.fullImage);

        Tools.setBoldFont(vh.title);
        Tools.setBoldFont(vh.bookTitle);
        Tools.setFont(vh.description);
        Tools.setBoldFont(vh.showMore);
        Tools.setFont(vh.fullDesc);
        Tools.setFont(vh.buy);

        Tools.setBoldFont(vh.allBooksTitle);
        Tools.setFont(vh.buy2);

        vh.title.setText(R.string.books);

        vh.scrollView.setVisibility(View.GONE);
        showProgressBar();

        if (VitrineAdapter.bookDesc != null) {
            vh.fullDesc.setVisibility(View.VISIBLE);
            vh.showMore.setVisibility(View.VISIBLE);

            vh.fullDesc.setText(VitrineAdapter.bookDesc);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                vh.fullDesc.setJustificationMode(Layout.JUSTIFICATION_MODE_INTER_WORD);
            }
            vh.showMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    vh.fullDesc.setMaxLines(100);
                    vh.showMore.setVisibility(View.INVISIBLE);
                }
            });
        }

        vh.buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<Pay> call = service.pay(prefManager.getAccessToken(), String.valueOf(VitrineAdapter.bookId), VitrineAdapter.bookModel);
                call.enqueue(new Callback<Pay>() {
                    @Override
                    public void onResponse(Call<Pay> call, final Response<Pay> response) {
                        hideProgressBar();
                        if (response.body() != null) {
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(response.body().getUrl()));
                            startActivity(browserIntent);
                            finish();
                        }
                        else {
                            Tools.displayErrorToast(getApplicationContext(), "به حساب کاربری خود وارد شوید");
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Call<Pay> call, Throwable t) {
                        hideProgressBar();
                        Tools.displayErrorToast(getApplicationContext(), getResources().getString(R.string.error));
                    }
                });
            }
        });

        vh.buy2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (VitrineAdapter.bookTitle.equals("درسنامه")) {
                    bookModel = "book";
                }
                if (VitrineAdapter.bookTitle.equals("دستنامه")) {
                    bookModel = "minibook";
                }
                if (VitrineAdapter.bookTitle.equals("سایر")) {
                    bookModel = "sayer";
                }

                GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
                Call<Pay> call = service.pay(prefManager.getAccessToken(), "all", bookModel);
                call.enqueue(new Callback<Pay>() {
                    @Override
                    public void onResponse(Call<Pay> call, final Response<Pay> response) {
                        hideProgressBar();
                        if (response.body() != null) {
                            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(response.body().getUrl()));
                            startActivity(browserIntent);
                            finish();
                        }
                        else {
                            Tools.displayErrorToast(getApplicationContext(), "به حساب کاربری خود وارد شوید");
                            finish();
                        }
                    }

                    @Override
                    public void onFailure(Call<Pay> call, Throwable t) {
                        hideProgressBar();
                        Tools.displayErrorToast(getApplicationContext(), getResources().getString(R.string.error));
                    }
                });
            }
        });

        if (VitrineAdapter.bookTitle.equals("درسنامه")) {
            fullImageUrl = "/storage/category/1.png";
        }
        if (VitrineAdapter.bookTitle.equals("دستنامه")) {
            fullImageUrl = "/public/img/dastname.png";
        }
        if (VitrineAdapter.bookTitle.equals("سایر")) {
            vh.allBooks.setVisibility(View.GONE);
            vh.buy2.setVisibility(View.GONE);
        }

        Glide.with(this)
                .load(RetrofitClientInstance.BASE_URL + VitrineAdapter.icon)
                .apply(new RequestOptions()
                        .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                        .diskCacheStrategy(DiskCacheStrategy.NONE))
                .transition(DrawableTransitionOptions.withCrossFade(150))
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        vh.scrollView.setVisibility(View.VISIBLE);
                        hideProgressBar();
                        return false;
                    }
                })
                .into(vh.imageView);

        Glide.with(this)
                .load(RetrofitClientInstance.BASE_URL_STORAGE + fullImageUrl)
                .apply(new RequestOptions()
                        .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                        .diskCacheStrategy(DiskCacheStrategy.NONE))
                .transition(DrawableTransitionOptions.withCrossFade(150))
                .listener(new RequestListener<Drawable>() {
                    @Override
                    public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                        vh.scrollView.setVisibility(View.VISIBLE);
                        hideProgressBar();
                        return false;
                    }
                })
                .into(vh.fullImage);

        if (VitrineAdapter.STATE == VitrineAdapter.BUY) {
            vh.info.setVisibility(View.VISIBLE);
            vh.buy.setVisibility(View.VISIBLE);
            vh.description.setText("شامل اطلاعات " + VitrineAdapter.quantity + " دارو ");
            vh.buy.setText(Tools.convertNumbersToPersian(VitrineAdapter.bookPrice) + " تومان\nسالیانه");
            vh.allBooksTitle.setText("تمام کتاب‌های " + VitrineAdapter.bookTitle);
            vh.buy2.setText(Tools.convertNumbersToPersian(VitrineAdapter.allBooksPrice) + " تومان\nسالیانه");
        }

        if (VitrineAdapter.STATE == VitrineAdapter.BUY_ALL) {
            vh.info.setVisibility(View.GONE);
            vh.buy.setVisibility(View.GONE);
            vh.allBooksTitle.setText("تمام کتاب‌های " + VitrineAdapter.bookTitle);
            vh.buy2.setText(Tools.convertNumbersToPersian(VitrineAdapter.allBooksPrice) + " تومان\nسالیانه");
        }

        vh.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

    }

    private void showProgressBar() {
        vh.progressbar.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar() {
        vh.progressbar.setVisibility(View.GONE);
    }

    private void checkNetwork() {
        if (Tools.isConnected(getApplicationContext())) {
            vh.internet_connection.setVisibility(View.GONE);
        } else {
            vh.internet_connection.setVisibility(View.VISIBLE);
            vh.tryAgain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onResume();
                }
            });
        }
    }

    private static class ViewHolder {
        ProgressBar progressbar;

        ImageView back;

        RelativeLayout internet_connection;
        TextView internetMessage;
        MaterialButton tryAgain;

        ScrollView scrollView;

        RelativeLayout info;
        ImageView imageView;
        TextView title;
        TextView bookTitle;
        TextView description;
        TextView showMore;
        TextView fullDesc;
        MaterialButton buy;

        RelativeLayout allBooks;
        TextView allBooksTitle;
        ImageView fullImage;
//        ListView gridView;
        MaterialButton buy2;
    }

}
