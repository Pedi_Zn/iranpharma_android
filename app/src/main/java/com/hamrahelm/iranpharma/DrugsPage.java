package com.hamrahelm.iranpharma;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.google.gson.JsonArray;
import com.hamrahelm.iranpharma.Crypto.AesEncryptDecrypt;
import com.hamrahelm.iranpharma.Crypto.AesEncryptionData;
import com.hamrahelm.iranpharma.adapter.DrugListAdapter2;
import com.hamrahelm.iranpharma.model.Logout;
import com.hamrahelm.iranpharma.model.Drug;
import com.hamrahelm.iranpharma.network.GetDataService;
import com.hamrahelm.iranpharma.network.RetrofitClientInstance;
import com.google.android.material.button.MaterialButton;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DrugsPage extends AppCompatActivity {

    public static ViewHolder vh;
    public int drugSize =0;
    private PrefManager prefManager;
    public int screenHeight;
    ArrayList<HashMap<String, String>> drugList = new ArrayList<>();
    HashMap<String, ArrayList<String>> table = new HashMap<String, ArrayList<String>>();

    Drug drug = new Drug();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drug_page);
        prefManager = new PrefManager(getApplicationContext());
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        screenHeight = displayMetrics.heightPixels / 11;
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        initViewHolder();
        checkNetwork();
    }

    private void initViewHolder() {
        vh = new ViewHolder();
        vh.progressbar = findViewById(R.id.progressbar);
        vh.title = findViewById(R.id.title);

        vh.back = findViewById(R.id.back);
        vh.internet_connection = findViewById(R.id.internet_connection);
        vh.internetMessage = findViewById(R.id.internetMessage);
        vh.tryAgain = findViewById(R.id.tryAgain);
        vh.drugDetailsList = findViewById(R.id.drugDetailsList);

        vh.image = findViewById(R.id.image);

        vh.drugNameFa = findViewById(R.id.drugNameFa);
        vh.drugNameEn = findViewById(R.id.drugNameEn);

        vh.listView = findViewById(R.id.listView);
        vh.drugId = findViewById(R.id.drugId);

        Tools.setBoldFont(vh.title);
        Tools.setFont(vh.drugNameFa);
        Tools.setFont(vh.drugNameEn);
        Tools.setFont(vh.drugId);

//        list view khali she
//        vh.listView.removeAllViews();

        vh.title.setText(DrugFragment.drugName);
        final String idString = String.valueOf(DrugFragment.drugId);
        vh.drugId.setText(idString);
        vh.back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        vh.drugDetailsList.setVisibility(View.GONE);

        showProgressBar();
        GetDataService service = RetrofitClientInstance.getRetrofitInstance().create(GetDataService.class);
        Call<Logout> call = service.drug(DrugFragment.drugId, prefManager.getAccessToken());
        call.enqueue(new Callback<Logout>() {
            @Override
            public void onResponse(Call<Logout> call, final Response<Logout> response) {
                hideProgressBar();
                if (response.body() != null) {
                    try {
                        String decrypt;
                        String decrypt1;
                        String aesDataString = new String(Base64.decode(response.body().getMessage().getBytes(), Base64.DEFAULT));
                        AesEncryptionData aesEncryptedData = new Gson().fromJson(aesDataString, AesEncryptionData.class);
                        decrypt = AesEncryptDecrypt.decrypt(AesEncryptDecrypt.key.getBytes(), aesEncryptedData.iv, aesEncryptedData.value, aesEncryptedData.mac);

                        String aesDataString1 = new String(Base64.decode(decrypt.getBytes(), Base64.DEFAULT));
                        AesEncryptionData aesEncryptedData1 = new Gson().fromJson(aesDataString1, AesEncryptionData.class);
                        decrypt1 = AesEncryptDecrypt.decrypt(prefManager.getKey().getBytes(), aesEncryptedData1.iv, aesEncryptedData1.value, aesEncryptedData1.mac);

                        JSONObject jObject = new JSONObject(decrypt1);
                        fillDrugsData(jObject);

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    if (drugList.isEmpty())
                        fillData();
                    vh.drugNameFa.setText(drug.getTitle());
                    vh.drugNameEn.setText(drug.getTitleEn());
                    LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) vh.listView.getLayoutParams();
                    params.height = screenHeight * drugSize;
                    vh.listView.setLayoutParams(params);

                    JSONArray tablesArray;
                    try {
                        tablesArray = new JSONArray(drug.getTables());
                        for (int i=0; i < tablesArray.length(); i++) {
                            JSONObject tableObject = tablesArray.getJSONObject(i);
                            String key = tableObject.getString("key");
                            String url = tableObject.getString("picture_url");
                            if(table.get(key) != null){
                                table.get(key).add(url);
                            }
                            else {
                                ArrayList a = new ArrayList<String>();
                                a.add(url);
                                table.put(key, a);
                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    vh.listView.setAdapter(new DrugListAdapter2(getApplicationContext(), drugList, vh.listView, drugSize, table, drug));

                    vh.drugDetailsList.setVisibility(View.VISIBLE);

                    if (drug.getPicture().equals("null")) {
                        vh.image.setImageResource(R.drawable.drug_img);
                    }
                    else {
                        Glide.with(getApplicationContext())
                            .load(RetrofitClientInstance.BASE_URL + drug.getPicture())
                            .apply(new RequestOptions()
                            .override(Target.SIZE_ORIGINAL, Target.SIZE_ORIGINAL)
                            .diskCacheStrategy(DiskCacheStrategy.NONE))
                            .transition(DrawableTransitionOptions.withCrossFade(150))
                            .listener(new RequestListener<Drawable>() {
                                @Override
                                public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                                    return false;
                                }

                                @Override
                                public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                                    hideProgressBar();
                                    return false;
                                }
                            })
                            .into(vh.image);
                    }
                }
            }

            @Override
            public void onFailure(Call<Logout> call, Throwable t) {
                hideProgressBar();
                Tools.displayErrorToast(getApplicationContext(), getResources().getString(R.string.error));
            }
        });
    }

    private void fillDrugsData(JSONObject jObject) {
        try {
            drug.setId(jObject.getString("id"));
            drug.setModelId(Integer.parseInt(jObject.getString("model_id")));
            drug.setTitle(jObject.getString("title"));
            drug.setTitleEn(jObject.getString("title_en"));
            drug.setTitleTejari(jObject.getString("title_tejari"));
            drug.setCureClass(jObject.getString("cure_class"));
            drug.setDosage(jObject.getString("dosage"));
            drug.setPrescribingConsumables(jObject.getString("prescribing_consumables"));
            drug.setSafetyTips(jObject.getString("safety_tips"));
            drug.setComplications(jObject.getString("complications"));
            drug.setContraindication(jObject.getString("contraindication"));
            drug.setAttention(jObject.getString("attention"));
            drug.setInteractions(jObject.getString("interactions"));
            drug.setPregnancy(jObject.getString("pregnancy"));
            drug.setFoodConsiderations(jObject.getString("food_considerations"));
            drug.setMonitoringParameters(jObject.getString("monitoring_parameters"));
            drug.setReferenceScope(jObject.getString("reference_scope"));
            drug.setPharmacodynamics(jObject.getString("pharmacodynamics"));
            drug.setAdditionalInformation(jObject.getString("additional_information"));
            drug.setCompoundProducts(jObject.getString("compound_products"));
            drug.setMedicalInformation(jObject.getString("medical_information"));
            drug.setSources(jObject.getString("sources"));
            drug.setUs(jObject.getString("us"));
            drug.setPicture(jObject.getString("picture"));
            drug.setPublish(jObject.getString("publish"));
            drug.setTables(jObject.getString("tables"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void fillData() {
        int subId = drug.getModelId();
        if (!drug.getCureClass().equals("null")) {
            if (subId >= 220 && subId <= 230) {
                HashMap<String, String> drugMap = new HashMap<>();
                drugMap.put(getResources().getString(R.string.cure_class2), drug.getCureClass());
                drugList.add(drugMap);
                drugSize++;
            }
            else if (subId == 231) {
                HashMap<String, String> drugMap = new HashMap<>();
                drugMap.put(getResources().getString(R.string.cure_class3), drug.getCureClass());
                drugList.add(drugMap);
                drugSize++;
            }
            else {
                HashMap<String, String> drugMap = new HashMap<>();
                drugMap.put(getResources().getString(R.string.cure_class), drug.getCureClass());
                drugList.add(drugMap);
                drugSize++;
            }
        }
        if (!drug.getDosage().equals("null")) {
            if (subId >= 220 && subId <= 230) {
                HashMap<String, String> drugMap = new HashMap<>();
                drugMap.put(getResources().getString(R.string.dosage2), drug.getDosage());
                drugList.add(drugMap);
                drugSize++;
            }
            else if (subId == 231) {
                HashMap<String, String> drugMap = new HashMap<>();
                drugMap.put(getResources().getString(R.string.dosage3), drug.getDosage());
                drugList.add(drugMap);
                drugSize++;
            }
            else if (subId == 241) {
                HashMap<String, String> drugMap = new HashMap<>();
                drugMap.put("قسمت مورد استفاده", drug.getDosage());
                drugList.add(drugMap);
                drugSize++;
            }
            else {
                HashMap<String, String> drugMap = new HashMap<>();
                drugMap.put(getResources().getString(R.string.dosage), drug.getDosage());
                drugList.add(drugMap);
                drugSize++;
            }
        }
        if (!drug.getPrescribingConsumables().equals("null")) {
            if (subId >= 220 && subId <= 230) {
                HashMap<String, String> drugMap = new HashMap<>();
                drugMap.put(getResources().getString(R.string.prescribing_consumables2), drug.getPrescribingConsumables());
                drugList.add(drugMap);
                drugSize++;
            }
            else if (subId == 231) {
                HashMap<String, String> drugMap = new HashMap<>();
                drugMap.put(getResources().getString(R.string.prescribing_consumables3), drug.getPrescribingConsumables());
                drugList.add(drugMap);
                drugSize++;
            }
            else if (subId == 241) {
                HashMap<String, String> drugMap = new HashMap<>();
                drugMap.put("مورد استفاده", drug.getPrescribingConsumables());
                drugList.add(drugMap);
                drugSize++;
            }
            else {
                HashMap<String, String> drugMap = new HashMap<>();
                drugMap.put(getResources().getString(R.string.prescribing_consumables), drug.getPrescribingConsumables());
                drugList.add(drugMap);
                drugSize++;
            }
        }
        if (!drug.getSafetyTips().equals("null")) {
            HashMap<String, String> drugMap = new HashMap<>();
            drugMap.put(getResources().getString(R.string.safety_tips), drug.getSafetyTips());
            drugList.add(drugMap);
            drugSize++;
        }
        if (!drug.getComplications().equals("null")) {
            if (subId >= 220 && subId <= 230) {
                HashMap<String, String> drugMap = new HashMap<>();
                drugMap.put(getResources().getString(R.string.complications2), drug.getComplications());
                drugList.add(drugMap);
                drugSize++;
            }
            else if (subId == 231) {
                HashMap<String, String> drugMap = new HashMap<>();
                drugMap.put(getResources().getString(R.string.complications2), drug.getComplications());
                drugList.add(drugMap);
                drugSize++;
            }
//            else if (subId == 241) {
//                HashMap<String, String> drugMap = new HashMap<>();
//                drugMap.put("", drug.getComplications());
//                drugList.add(drugMap);
//                drugSize++;
//            }
            else {
                HashMap<String, String> drugMap = new HashMap<>();
                drugMap.put(getResources().getString(R.string.complications), drug.getComplications());
                drugList.add(drugMap);
                drugSize++;
            }
        }
        if (!drug.getContraindication().equals("null")) {
            if (subId >= 220 && subId <= 230) {
                HashMap<String, String> drugMap = new HashMap<>();
                drugMap.put(getResources().getString(R.string.contraindication2), drug.getContraindication());
                drugList.add(drugMap);
                drugSize++;
            }
            else if (subId == 231) {
                HashMap<String, String> drugMap = new HashMap<>();
                drugMap.put(getResources().getString(R.string.contraindication2), drug.getContraindication());
                drugList.add(drugMap);
                drugSize++;
            }
            else if (subId == 241) {
                HashMap<String, String> drugMap = new HashMap<>();
                drugMap.put(getResources().getString(R.string.contraindication2), drug.getContraindication());
                drugList.add(drugMap);
                drugSize++;
            }
            else {
                HashMap<String, String> drugMap = new HashMap<>();
                drugMap.put(getResources().getString(R.string.contraindication), drug.getContraindication());
                drugList.add(drugMap);
                drugSize++;
            }
        }
        if (!drug.getAttention().equals("null")) {
            if (subId >= 220 && subId <= 230) {
                HashMap<String, String> drugMap = new HashMap<>();
                drugMap.put(getResources().getString(R.string.attention2), drug.getAttention());
                drugList.add(drugMap);
                drugSize++;
            }
            else {
                HashMap<String, String> drugMap = new HashMap<>();
                drugMap.put(getResources().getString(R.string.attention), drug.getAttention());
                drugList.add(drugMap);
                drugSize++;
            }
        }
        if (!drug.getInteractions().equals("null")) {
            HashMap<String, String> drugMap = new HashMap<>();
            drugMap.put(getResources().getString(R.string.interactions), drug.getInteractions());
            drugList.add(drugMap);
            drugSize++;
        }
        if (!drug.getPregnancy().equals("null")) {
            HashMap<String, String> drugMap = new HashMap<>();
            drugMap.put(getResources().getString(R.string.pregnancy), drug.getPregnancy());
            drugList.add(drugMap);
            drugSize++;
        }
        if (!drug.getFoodConsiderations().equals("null")) {
            HashMap<String, String> drugMap = new HashMap<>();
            drugMap.put(getResources().getString(R.string.food_considerations), drug.getFoodConsiderations());
            drugList.add(drugMap);
            drugSize++;
        }
        if (!drug.getMonitoringParameters().equals("null")) {
            if (subId >= 220 && subId <= 230) {
                HashMap<String, String> drugMap = new HashMap<>();
                drugMap.put(getResources().getString(R.string.monitoring_parameters2), drug.getMonitoringParameters());
                drugList.add(drugMap);
                drugSize++;
            }
            else if (subId == 231) {
                HashMap<String, String> drugMap = new HashMap<>();
                drugMap.put(getResources().getString(R.string.monitoring_parameters3), drug.getMonitoringParameters());
                drugList.add(drugMap);
                drugSize++;
            }
            else {
                HashMap<String, String> drugMap = new HashMap<>();
                drugMap.put(getResources().getString(R.string.monitoring_parameters), drug.getMonitoringParameters());
                drugList.add(drugMap);
                drugSize++;
            }
        }
        if (!drug.getReferenceScope().equals("null")) {
            HashMap<String, String> drugMap = new HashMap<>();
            drugMap.put(getResources().getString(R.string.reference_scope), drug.getReferenceScope());
            drugList.add(drugMap);
            drugSize++;
        }
        if (!drug.getPharmacodynamics().equals("null")) {
            if (subId == 231) {
                HashMap<String, String> drugMap = new HashMap<>();
                drugMap.put(getResources().getString(R.string.pharmacodynamics3), drug.getPharmacodynamics());
                drugList.add(drugMap);
                drugSize++;
            }
            else {
                HashMap<String, String> drugMap = new HashMap<>();
                drugMap.put(getResources().getString(R.string.pharmacodynamics), drug.getPharmacodynamics());
                drugList.add(drugMap);
                drugSize++;
            }
        }
        if (!drug.getAdditionalInformation().equals("null")) {
            if (subId == 231) {
                HashMap<String, String> drugMap = new HashMap<>();
                drugMap.put(getResources().getString(R.string.additional_information3), drug.getAdditionalInformation());
                drugList.add(drugMap);
                drugSize++;
            }
            else {
                HashMap<String, String> drugMap = new HashMap<>();
                drugMap.put(getResources().getString(R.string.additional_information), drug.getAdditionalInformation());
                drugList.add(drugMap);
                drugSize++;
            }
        }
        if (!drug.getCompoundProducts().equals("null")) {
            if (subId == 231) {
                HashMap<String, String> drugMap = new HashMap<>();
                drugMap.put(getResources().getString(R.string.compound_products3), drug.getCompoundProducts());
                drugList.add(drugMap);
                drugSize++;
            }
            else {
                HashMap<String, String> drugMap = new HashMap<>();
                drugMap.put(getResources().getString(R.string.compound_products), drug.getCompoundProducts());
                drugList.add(drugMap);
                drugSize++;
            }
        }
        if (!drug.getMedicalInformation().equals("null")) {
            HashMap<String, String> drugMap = new HashMap<>();
            drugMap.put(getResources().getString(R.string.medical_information), drug.getMedicalInformation());
            drugList.add(drugMap);
            drugSize++;
        }
        if (!drug.getSources().equals("null")) {
             if (subId == 241) {
                HashMap<String, String> drugMap = new HashMap<>();
                drugMap.put("منابع", drug.getSources());
                drugList.add(drugMap);
                drugSize++;
             }
             else{
                 HashMap<String, String> drugMap = new HashMap<>();
                 drugMap.put(getResources().getString(R.string.sources), drug.getSources());
                 drugList.add(drugMap);
                 drugSize++;
             }


        }
        if (!drug.getUs().equals("null")) {
            if (subId == 231) {
                HashMap<String, String> drugMap = new HashMap<>();
                drugMap.put(getResources().getString(R.string.us3), drug.getUs());
                drugList.add(drugMap);
                drugSize++;
            }
            else {
                HashMap<String, String> drugMap = new HashMap<>();
                drugMap.put(getResources().getString(R.string.us), drug.getUs());
                drugList.add(drugMap);
                drugSize++;
            }
        }
    }

    private void showProgressBar() {
        vh.progressbar.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar() {
        vh.progressbar.setVisibility(View.INVISIBLE);
    }

    private void checkNetwork() {
        if (Tools.isConnected(getApplicationContext())) {
            vh.internet_connection.setVisibility(View.GONE);
        } else {
            vh.internet_connection.setVisibility(View.VISIBLE);
            vh.tryAgain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onPostResume();
                }
            });
        }
    }

    public static class ViewHolder {
        TextView title;
        ProgressBar progressbar;

        ImageView back;

        RelativeLayout internet_connection;
        TextView internetMessage;
        MaterialButton tryAgain;

        ImageView image;

        TextView drugNameFa;
        TextView drugNameEn;
        TextView drugId;

        LinearLayout drugDetailsList;

        ListView listView;
    }
}
