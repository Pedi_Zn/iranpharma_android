package com.hamrahelm.iranpharma.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class DrugCategoriesDatabase extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 7;
    private static final String DATABASE_NAME = "drugcategories.db";
    public static final String DRUG_CATEGORIES_TABLE_NAME = "drugcategories";

    public static final String CATEGORY_COLUMN_ID = "id";
    public static final String CATEGORY_COLUMN_NAME  = "name";

    public DrugCategoriesDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL("CREATE TABLE " + DRUG_CATEGORIES_TABLE_NAME + " (" +
                CATEGORY_COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                CATEGORY_COLUMN_NAME + " TEXT" + ")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DRUG_CATEGORIES_TABLE_NAME);
        onCreate(sqLiteDatabase);
    }

    public void add(values lang) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(CATEGORY_COLUMN_NAME, lang.getValue()); // Contact Name

        // Inserting Row
        db.insert(DRUG_CATEGORIES_TABLE_NAME, null, values);
        db.close(); // Closing database connection
    }

    // Getting single contact
    public values getContact(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(DRUG_CATEGORIES_TABLE_NAME, new String[] { CATEGORY_COLUMN_ID,
                        CATEGORY_COLUMN_NAME }, CATEGORY_COLUMN_ID + "=?", new String[] { String.valueOf(id) },
                null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        values lang = new values(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1));

        return lang;
    }

    // Getting All data
    public List<values> getAllvalues() {
        List<values> languageList = new ArrayList<values>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + DRUG_CATEGORIES_TABLE_NAME;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                values lang = new values();
                lang.setId(Integer.parseInt(cursor.getString(0)));
                lang.setValue(cursor.getString(1));
                languageList.add(lang);
            } while (cursor.moveToNext());
        }

        return languageList;
    }

    // Updating single record
    public int update(values lang) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(CATEGORY_COLUMN_NAME, lang.getValue());

        // updating row
        return db.update(DRUG_CATEGORIES_TABLE_NAME, values, CATEGORY_COLUMN_ID + " = ?",
                new String[] { String.valueOf(lang.getId()) });
    }
}
