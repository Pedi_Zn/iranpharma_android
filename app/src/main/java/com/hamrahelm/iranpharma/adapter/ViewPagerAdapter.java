package com.hamrahelm.iranpharma.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.viewpager.widget.PagerAdapter;

import com.bumptech.glide.Glide;
import com.hamrahelm.iranpharma.R;
import com.jsibbold.zoomage.ZoomageView;

import java.util.List;

public class ViewPagerAdapter extends PagerAdapter {
    Context c;
    private List<String> _imagePaths;
    private LayoutInflater inflater;
    public ViewPagerAdapter(Context c, List<String> imagePaths) {
        this._imagePaths = imagePaths;
        this.c = c;
    }
    @Override
    public int getCount() {
        return this._imagePaths.size();
    }
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == (object);
    }
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView imgDisplay;
        ZoomageView zoomageView;
        inflater = (LayoutInflater)c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View viewLayout = inflater.inflate(R.layout.product_viewpager_image, container,
                false);
//        imgDisplay = viewLayout.findViewById(R.id.image);

        zoomageView = viewLayout.findViewById(R.id.myZoomageView);

        Glide.with(c).load(_imagePaths.get(position)).into(zoomageView);
        (container).addView(viewLayout);
        return viewLayout;
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        (container).removeView((RelativeLayout) object);
    }
}
