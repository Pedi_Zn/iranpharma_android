package com.hamrahelm.iranpharma.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DrugCard {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("title_en")
    @Expose
    private String titleEn;
    @SerializedName("title_tejari")
    @Expose
    private Object titleTejari;
    @SerializedName("model")
    @Expose
    private String model;
    @SerializedName("model_id")
    @Expose
    private String model_Id;
    @SerializedName("has_medicine")
    @Expose
    private Boolean hasMedicine;
    @SerializedName("icon")
    @Expose
    private String icon;

    public Boolean getHasMedicine() {
        return hasMedicine;
    }

    public void setHasMedicine(Boolean hasMedicine) {
        this.hasMedicine = hasMedicine;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitleEn() {
        return titleEn;
    }

    public void setTitleEn(String titleEn) {
        this.titleEn = titleEn;
    }

    public Object getTitleTejari() {
        return titleTejari;
    }

    public void setTitleTejari(Object titleTejari) {
        this.titleTejari = titleTejari;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getModel_Id() {
        return model_Id;
    }

    public void setModel_Id(String model_Id) {
        this.model_Id = model_Id;
    }

}