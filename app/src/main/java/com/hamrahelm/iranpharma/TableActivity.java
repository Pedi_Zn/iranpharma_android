package com.hamrahelm.iranpharma;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.hamrahelm.iranpharma.adapter.ViewPagerAdapter;

import java.util.ArrayList;
import java.util.List;

public class TableActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table);
        Intent intent = getIntent();

        ImageView back = findViewById(R.id.back);
        TextView title = findViewById(R.id.title);

        Tools.setBoldFont(title);

        title.setText(intent.getStringExtra("title"));

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        ViewPager viewpager = (ViewPager) findViewById(R.id.pager);
        ViewPagerAdapter adapter = new ViewPagerAdapter (TableActivity.this, intent.getStringArrayListExtra("urlList"));
        viewpager.setAdapter(adapter);
    }
}